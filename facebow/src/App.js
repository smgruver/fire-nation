import React from 'react';
import './bow.css';
import './App.css';
import ReactRouter from './Routes/mainRouter';

function App() {
  return (
    <div className="App">
      <ReactRouter></ReactRouter>
    </div>
  );
}

export default App;
