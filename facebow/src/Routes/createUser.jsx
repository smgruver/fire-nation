import React from 'react';
import './../loginPages.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { s3upload } from './bucket'
import axios from 'axios';
class CreateUser extends React.Component {
    constructor(props) {
        super(props)
        console.log(this.props.current_id);
        if(this.props.current_id){
            this.props.history.push("/home");
        }
        this.state = {
            file:null,
            message: "",
            username: "",
            password: "",
            repassword: "",
            firstname: "",
            lastname: "",
            email: "",
            fileName:null
        }
        this.handleImage = this.handleImage.bind(this);
        this.deleteImage = this.deleteImage.bind(this);
    }

    async create(event) {
       let pic
        if(!this.state.file){
            pic="https://firenationbucket.s3.us-east-2.amazonaws.com/avatar.png";
        }
        else{
           let key=this.state.file.split("/")[3];
           pic=s3upload(this.state.fileName, "NewUser", key) ;
        }

        let checkuser=await axios({
                method: 'get',
                  url: 'http://localhost:8080/FireNation/api/users/getUserByUsername?username='+this.state.username,
                     withCredentials: true
             });
        let checkemail=await axios({
                method: 'get',
                  url: 'http://localhost:8080/FireNation/api/users/getUserByEmail?email='+this.state.email,
                     withCredentials: true
             });

        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       if(!re.test(String(this.state.email).toLowerCase())){
        this.setState({ message: "Invalid email format" });
        return;
       }
        if(checkuser.data){
            this.setState({ message: "Username already exsist" });
            return;
        }
        if(checkemail.data){
            this.setState({ message: "Email already exsist" });
            return;
        }

        if(!this.state.username || !this.state.password || !this.state.repassword || !this.state.firstname || !this.state.lastname || !this.state.email)
      {
        this.setState({ message: "user information can't be empty" });
        return;
      }
      if(this.state.password!==this.state.repassword){
        this.setState({ message: "two password must be match" });
        return;
      }
      const responsePayload = await axios({
        method: 'post',
        url: 'http://localhost:8080/FireNation/api/users/createUser',
        data: {
      'username': this.state.username,
      'password': this.state.password,
      'firstName': this.state.firstname,
      'lastName': this.state.lastname,
      'email': this.state.email,
      'image': pic,
     },
        withCredentials: true,
    });
    console.log(responsePayload);
    if(responsePayload.data==="Success"){
        this.setState({ message: "Register Successful" });
    }else{
        this.setState({ message: "Fail to Register" });
    }
    
    }

    handleImage(event) {
        if(event.target.files[0]){
        this.setState({
            file: URL.createObjectURL(event.target.files[0]),
            fileName:event.target.files[0]
      })
      event.target.value=null;
      }
    }
    deleteImage(event){
        this.setState({
            file: null
      })
    }
    updateUser(e) {
        this.setState({ username: e.target.value });
    }

    updatePass(e) {
        this.setState({ password: e.target.value });
    }
    updatePassSecond(e) {
        this.setState({ repassword: e.target.value });
    }

    updateFirst(e) {
        this.setState({ firstname: e.target.value });
    }

    updateLast(e) {
        this.setState({ lastname: e.target.value });
    }

    updateEm(e) {
        this.setState({ email: e.target.value });
    }

    //user, password
    render() {
        return (
            <React.Fragment>
                <div className="card" id='newACard'>
                <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <h1 className="titleW">New Account</h1>

                    {(this.state.file)&&<img className="postImage" src={this.state.file} alt="" />}
                    
                    <br />
                      <input type="file" id="test" accept="image/x-png,image/gif,image/jpeg" onChange={this.handleImage}/>
                    {(this.state.file)&&<button className="btn btn-primary" onClick={this.deleteImage}>Delete Image</button>}
                    <div className="loginSmall">
                        Username:
                    </div>
                    <input type="username" className="inField" id="usernameC" onChange={(value) => this.updateUser(value)} value={this.state.username}></input>
                    <div className="loginSmall">
                        Password:
                    </div>
                    <input type="password" className="inField" id="passwordC" onChange={(value) => this.updatePass(value)} value={this.state.password}></input>
                    <div className="loginSmall">
                        Comfirmeed Password:
                    </div>
                    <input type="password" className="inField" id="passwordC" onChange={(value) => this.updatePassSecond(value)} value={this.state.repassword}></input>
                    <div className="loginSmall">
                        First name:
                    </div>
                    <input type="username" className="inField" id="firstNC" onChange={(value) => this.updateFirst(value)} value={this.state.firstname}></input>
                    <div className="loginSmall">
                        Last Name:
                    </div>
                    <input type="username" className="inField" id="lastNC" onChange={(value) => this.updateLast(value)} value={this.state.lastName}></input>
                    <div className="loginSmall">
                        Email:
                    </div>
                    <input type="email" className="inField" id="emailC" onChange={(value) => this.updateEm(value)} value={this.state.email}></input>

                    <div className="Warning">
                        {this.state.message}
                    </div>
                    <button type="button" className="loginButton" onClick={() => this.create()}>Create User</button>
                    <Link to="/">
                        <button className="btn btn-success" style={{marginBottom:'8px'}}>Back</button>
                    </Link>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                </div>
            </React.Fragment>
        )
    }

}
const mapStateToProps = state => {
    return {
        current_id: state.valid_userId,
    }
}; 

export default connect(mapStateToProps)(CreateUser);