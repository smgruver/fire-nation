import React from 'react';
import './../loginPages.css';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import axios from 'axios';

class LoginPage extends React.Component {
    constructor(props) {
        super(props)
        if(this.props.current_id){
            this.props.history.push("/home");
        }
        this.state = {
            loginId: "",
            passwordId: "",
            warnMessage:""
        }
    }
    
    async checkLogin(event) {
        console.log(this.state.loginId);
        console.log(this.state.passwordId);

      if(!this.state.loginId || !this.state.passwordId){
            this.setState({ warnMessage: "Please fill in the username and password"});
       return;
      }

//////////////////////////////////////////Missing ERROR HANDLIGN FOR LOGIN////////////////////////////////////////////
const responsePayload = await axios({
        method: 'post',
        url: 'http://localhost:8080/FireNation/api/login',
        data: {
      'username': this.state.loginId,
      'password': this.state.passwordId,
        },
        withCredentials: true,
    });
     let userinfo= await responsePayload
     console.log(userinfo.status);
     this.props.logIn(userinfo.data.id);
    if(userinfo.data.id){
     this.props.history.push('/home');
    }
    else{
        this.setState({ warnMessage: "Password is invalid"});
    }

    }


    updateLogin(e) {
        this.setState({ loginId: e.target.value });
    }

    updatePass(e) {
        this.setState({ passwordId: e.target.value });
    }

    render() {
        return (
            <React.Fragment>
                <div className="card" id='loginCard'>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>

                    <h1 className="titleW">Login</h1>
                    <div className="loginSmall">
                        Name:
                    </div>
                    <input type="username" className="inField" id="usernameId" onChange={(value) => this.updateLogin(value)} value={this.state.loginId}></input>

                    <div className="loginSmall">
                        Password:
                    </div>
                    <input type="password" className="inField" id="passwordId" onChange={(value) => this.updatePass(value)} value={this.state.passwordId}></input>

                 
                    <button type="button" className="loginButton" onClick={() => this.checkLogin()}>Login</button>
                    <div className="warnMessage">{this.state.warnMessage}</div>
                    <Link to="/createUser">
                        <button className="createUse">Create User</button>
                    </Link>
                    <Link to="/resetPass">
                        <button className="resetPassBtn">Reset Password</button>
                    </Link>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                </div>

            </React.Fragment>
        )
    }

}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (name) => dispatch({ type: 'LOGIN', user: name }),
        logOut: () => dispatch({ type: 'LOGOUT'})
    }
}

const mapStateToProps = state => {
    return {
        current_id: state.valid_userId,
    }
}; 

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LoginPage));;