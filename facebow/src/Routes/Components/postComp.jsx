import React from 'react';
import axios from 'axios';

async function test(id, props){
  const responsePayload =  await axios({
    method: 'post',
    url: 'http://localhost:8080/FireNation/api/posts/like/'+id,
    withCredentials: true,
});
  console.log(responsePayload);
  props.rerenderParentCallback();

}
 function PostComp(props) {
   //console.log(props.list[0].myAuthor.id);
   console.log(props.list);
  return (
    <div>
      {props.list.map((element) => <div key={element.id} className="postComp">
        <h5 className="time">{element.creationDate.substring(0, 19).replace("@", " ")}</h5>
        <img className="post_user_image" src={element.myAuthor.image} alt=""></img>
        <h6>{element.myAuthor.firstName} {element.myAuthor.lastName}</h6>
        <br/>
        <p className="contentP">{element.content}</p>
        <div>
          {(element.image) && <img className="post_content_image" src={element.image} alt=""></img>}
        </div>

      <div><button onClick={() => test(element.id, props)} className="likeButton">Like</button><span>{element.likes.length} person Like this post</span></div>
      </div>)}

    </div>
  )
}

export default PostComp;