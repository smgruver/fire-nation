import React from 'react';
import './../loginPages.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import axios from 'axios';
class PassRe extends React.Component {
    constructor(props) {
        super(props)
        if (this.props.current_id) {
            this.props.history.push("/home");
        }
        this.state = {
            message: "",
            email: ""
        }
    }

    async sendEmail(event) {
        if (!this.state.email) {
            this.setState({ message: "Email can't be empty" });
            return;
        }

        let checkemail=await axios({
            method: 'get',
              url: 'http://localhost:8080/FireNation/api/users/getUserByEmail?email='+this.state.email,
                 withCredentials: true
         });
         if(!checkemail.data){
            this.setState({ message: "Email not exsist" });
            return;
        }
        console.log(this.state.email);
        let Reset=await axios({
            method: 'patch',
              url: 'http://localhost:8080/FireNation/api/users/resetPassword',
              data: {
                'input': this.state.email
             },
                 withCredentials: true
         });
         console.log(Reset);
        //Checks if valid(with backend)
        this.setState({ message: "Email set to reset password" });
    }

    updateEm(e) {
        this.setState({ email: e.target.value });
    }


    render() {
        return (
            <React.Fragment>
                <div className="card" id='loginCard'>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <h1 className="titleW">Reset Password</h1>
                    <div className="loginSmall">
                        Email:
                    </div>
                    <input type="email" className="inField" id="usernameId" onChange={(value) => this.updateEm(value)} value={this.state.email}></input>

                    <div className="Warning">
                        {this.state.message}
                    </div>
                    <button type="button" className="loginButton" onClick={() => this.sendEmail()}>Reset</button>
                    <Link to="/">
                        <button className="btn btn-danger">Back</button>
                    </Link>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                </div>
            </React.Fragment>
        )
    }

}
const mapStateToProps = state => {
    return {
        current_id: state.valid_userId,
    }
};
export default connect(mapStateToProps)(PassRe);