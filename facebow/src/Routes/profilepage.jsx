import React from 'react';
import NavBarComp from './Components/navbarComp';
import PostComp from './Components/postComp';
import { connect } from 'react-redux';
import axios from 'axios';
class ProfileRoute extends React.Component {
   constructor(props) {
      super(props);
    console.log(this.props.location.state.id)
      if(!this.props.current_id){
         this.props.history.push("/");
     }
      this.state = { 
         userInfo: null,
         postList:null,
         mount:false 
      }
      this.rerenderParentCallback = this.rerenderParentCallback.bind(this);
   }
   async componentDidMount(){
       
      this.setState({
              userInfo: await this.getuserInfo(),
              postList: await this.getlist(),
              mount:true
          })
  }
  async getlist(){
   let getlist=await axios({
           method: 'get',
           url: 'http://localhost:8080/FireNation/api/posts/getPostsByAuthor?author='+this.props.location.state.id,
           withCredentials: true,
       });
   return getlist.data;
}
async rerenderParentCallback() {
   this.setState({
       postList: await this.getlist(),
   });
 }
  async getuserInfo(){
   let test= await axios({
       method: 'get',
       url: 'http://localhost:8080/FireNation/api/users/getUserById?id='+this.props.location.state.id,
       withCredentials: true,
   });
   return test.data  
}
   render() {
      if(!this.state.mount){
         return(
             <div>Loading</div>
         )
     }

    return (
      <div className="home">
      <div className="header">
         <NavBarComp props={this.props}></NavBarComp>
      </div>   
      <div className="profileUsercontainer">
         <div className="profileUserleft">
        <h2>User Information</h2> 
        <div>
        <img src={this.state.userInfo.image} alt="Cinque Terre" className="userPic"></img>
       <p className="leftText"><b>Name:</b> {this.state.userInfo.firstName} {this.state.userInfo.lastName} </p>
        </div >
        <p className="leftText"><b>Email:</b> {this.state.userInfo.email}</p>
        </div>
        <div className="postUsercenter">
        <PostComp list={this.state.postList} rerenderParentCallback={this.rerenderParentCallback}></PostComp>
        </div>
    
   
      </div>
        
    </div>
    )
    }
}

const mapStateToProps = state => {
   return {
       current_id: state.valid_userId,
   }
}; 

    
    export default  connect(mapStateToProps)(ProfileRoute);