import React from 'react';
import NavBarComp from './Components/navbarComp';
import { connect } from 'react-redux';
import './../loginPages.css';
import { s3upload } from './bucket';
import axios from 'axios';
class SettingPage extends React.Component {
    constructor(props) {
        super(props)
        if (!this.props.current_id) {
            this.props.history.push("/");
        }

        this.state = {
            message: "",
            username: "",
            firstname: "",
            lastname: "",
            email: "",
            userInfo: null,
            file: null,
            fileName:null,
            mount:false 
        }
        this.handleImage = this.handleImage.bind(this);
    }
    async componentDidMount(){
       
        this.setState({
                userInfo: await this.getUserInfo(),
                mount:true
            })
        this.setState({
            username:this.state.userInfo.username,
            firstname:this.state.userInfo.firstName,
            lastname:this.state.userInfo.lastName,
            email:this.state.userInfo.email,
            file:this.state.userInfo.image
        })
    }
    async getUserInfo() {
        /** Get user information in here */
        let test= await axios({
            method: 'get',
            url: 'http://localhost:8080/FireNation/api/getUser',
            withCredentials: true,
        });
        return test.data   
     }


    async create(event) {
        //Checks if valid(with backend)
        let pic

        if (this.state.fileName) {
            let key = this.state.file.split("/")[3];
            pic=s3upload(this.state.fileName, "NewUser", key);
        }
        else{
            pic=this.state.userInfo.image
        }
        if(!this.state.username || !this.state.firstname || !this.state.lastname || !this.state.email){
            this.setState({ message: "All field must be fill in" });
            return;
        }
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
       if(!re.test(String(this.state.email).toLowerCase())){
        this.setState({ message: "Invalid email format" });
        return;
       }
        if(this.state.email!==this.state.userInfo.email){
            let checkemail=await axios({
                method: 'get',
                  url: 'http://localhost:8080/FireNation/api/users/getUserByEmail?email='+this.state.email,
                     withCredentials: true
             });
             if(checkemail.data){
                this.setState({ message: "Email already exsist" });
                return;
            }
        }
        console.log(this.state.userInfo.username);
        const responsePayload = await axios({
            method: 'patch',
            url: 'http://localhost:8080/FireNation/api/users/editUser',
            data: {
          'username': this.state.username,
          'firstName': this.state.firstname,
          'lastName': this.state.lastname,
          'email': this.state.email
            },
            withCredentials: true,
        });
        this.setState({ message: responsePayload.data });
    
    }
    handleImage(event) {
        if (event.target.files[0]) {
            this.setState({
                file: URL.createObjectURL(event.target.files[0]),
                fileName:event.target.files[0]
            })
            event.target.value = null;
        }
    }

    updateUser(e) {
        this.setState({ username: e.target.value });
    }

    updateFirst(e) {
        this.setState({ firstname: e.target.value });
    }

    updateLast(e) {
        this.setState({ lastname: e.target.value });
    }

    updateEm(e) {
        this.setState({ email: e.target.value });
    }

    goBack() {
        this.props.history.goBack()
    }

    //user, password
    render() {
       
        if(!this.state.mount){
            return(
                <div>Loading</div>
            )
        }
        return (
            
            <React.Fragment>
                <div>
                    <NavBarComp></NavBarComp>
                    <div className="card" id='settingBody'>
                        <h1 className="titleS">Update UserInfo</h1>
                        <img className="postImage" src={this.state.file} alt="" />

                        <br />
                        <div className="setSmall">
                            Username:
                        </div>

                        <input type="username" className="inField" id="usernameC"   onChange={(value) => this.updateUser(value)}  value={this.state.username}/>
                        <div className="setSmall">
                            First name:
                        </div>
                        <input type="username" className="inField" id="firstNC" onChange={(value) => this.updateFirst(value)} value={this.state.firstname} />
                        <div className="setSmall">
                            Last Name:
                        </div>
                        <input type="username" className="inField" id="lastNC" onChange={(value) => this.updateLast(value)} value={this.state.lastname} />
                        <div className="setSmall">
                            Email:
                        </div>
                        <input type="email" className="inField" id="emailC" onChange={(value) => this.updateEm(value)} value={this.state.email} />

                        <div className="Warning">
                            {this.state.message}
                        </div>
                        <button type="button" className="loginButton" onClick={() => this.create()}>Update Settings</button>

                        <button className="btn backButton" onClick={() => this.goBack()}>Back</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

const mapStateToProps = state => {
    return {
        current_id: state.valid_userId
    }
};

export default connect(mapStateToProps)(SettingPage);