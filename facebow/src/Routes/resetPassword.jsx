import React from 'react';
import NavBarComp from './Components/navbarComp';
import { connect } from 'react-redux';
import './../loginPages.css';
import axios from 'axios';
class ResetPassword extends React.Component {
    constructor(props) {
        super(props)
        if (!this.props.current_id) {
            this.props.history.push("/");
        }

        this.state = {
            oldPassword: "",
            newPassword: "",
            reNewPassword: "",
            message: ""
        }
    }



    async create(event) {
        console.log(this.state.oldPassword)
        console.log(this.state.newPassword)
        console.log(this.state.reNewPassword)
        if (!this.state.oldPassword || !this.state.newPassword || !this.state.reNewPassword) {
            this.setState({ message: "All field can't be empty" });
            return;
        }
        if (this.state.newPassword !== this.state.reNewPassword) {
            this.setState({ message: "Two new password not match" });
            return;
        }

        const responsePayload = await axios({
            method: 'patch',
            url: 'http://localhost:8080/FireNation/api/users/editUserPassword',
            data: {
          'oldPassword': this.state.oldPassword,
          'newPassword': this.state.newPassword,
            },
            withCredentials: true,
        });
        console.log(responsePayload.data);
      //  this.setState({ message: responsePayload.data });
      if(responsePayload.data){
        this.setState({ message: "success to change" });
      }
      else{
        this.setState({ message: "fail to change password" });
      }

    }

    oldPassword(e) {
        this.setState({ oldPassword: e.target.value });
    }

    newPassword(e) {
        this.setState({ newPassword: e.target.value });
    }

    reNewPassword(e) {
        this.setState({ reNewPassword: e.target.value });
    }

    goBack() {
        this.props.history.goBack()
    }

    //user, password
    render() {
        return (
            <React.Fragment>
                <div>
                    <NavBarComp></NavBarComp>
                    <div className="card" id='settingBody'>
                        <h1 className="titleS">Reset password</h1>
                        <div className="setSmall">
                            Old Password:
                    </div>
                        <input type="password" className="inField" onChange={(value) => this.oldPassword(value)}></input>
                        <div className="setSmall">
                            New Password:
                    </div>
                        <input type="password" className="inField" onChange={(value) => this.newPassword(value)} ></input>
                        <div className="setSmall">
                            Retype password:
                    </div>
                        <input type="password" className="inField" onChange={(value) => this.reNewPassword(value)} ></input>

                        <div className="Warning titleS">
                            {this.state.message}
                        </div>
                        <button type="button" className="loginButton" onClick={() => this.create()}>Update Settings</button>

                        <button className="btn backButton" onClick={() => this.goBack()}>Back</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

const mapStateToProps = state => {
    return {
        current_id: state.valid_userId
    }
};

export default connect(mapStateToProps)(ResetPassword);