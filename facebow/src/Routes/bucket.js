// import entire SDK
// import AWS from 'aws-sdk';
// // import AWS object without services
// import AWS from 'aws-sdk/global';
// import individual service
// import S3 from 'aws-sdk/clients/s3';
// import {config} from 'aws-sdk';

import { isValidElement } from 'react';

/**
 * @description This file sends a picture to S3 after checking if valid picture
 * 
 * @returns returns a string that is the pictures url
 * @param {*} file the file of the picture, must have a valid file.name
 * @param {*} user the users unique identifier
 * @param {*} ending the timestamp?
 */
export function s3upload(file, user, ending) {
    if(undefined==file.name)
        return;

    //If valid file, saves on S3    
    var AWS = require('aws-sdk');
    
    AWS.config.update({
        region: 'us-east-2',
        credentials: new AWS.CognitoIdentityCredentials({
            IdentityPoolId: 'us-east-2:c245b024-46e9-456e-967c-975f13824f74',
        }),
    });
    
    var s3 = new AWS.S3({
        apiVersion: '2006-03-01',
        params: { Bucket: 'firenationbucket' }
    });
    
    var filePath = user + '-bucket-path/'+ending+".png";
    var fileUrl = 'https://firenationbucket.s3.us-east-2.amazonaws.com/' + filePath;
    s3.upload({
        Key: filePath,
        Body: file,
        ACL: 'public-read'
    }, function (err, data) {
        if (err) {
            console.log("THERE IS AN ERROR");
            console.log(err);
            
        } else if (data){
            fileUrl = data.Location;
        }        
    })
    /* .on('httpUploadProgress', function (progress) {
    var uploaded = parseInt((progress.loaded * 100) / progress.total);
   //  $("progress").attr('value', uploaded);
  }); */
    return fileUrl;
};