import React from 'react';
import NavBarComp from './Components/navbarComp';
import PostComp from './Components/postComp';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { s3upload } from './bucket';
import axios from 'axios';
class HomeRoute extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userPost: "",
            file: null,
            searchValue: "",
            userInfo: null,
            userList: null,
            filterList: null,
            postList:null,
            mount:false,
            fileName:null
        };

        if (!this.props.current_id) {
            this.props.history.push("/");
        }

        this.handlePost = this.handlePost.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleImage = this.handleImage.bind(this);
        this.handleProfile = this.handleProfile.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.deleteImage = this.deleteImage.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.rerenderParentCallback = this.rerenderParentCallback.bind(this);
    }

    async componentDidMount(){
       
        this.setState({
                userInfo: await this.getuserInfo(),
                userList:await this.getUserList(),
                filterList: await this.getUserList(),
                postList: await this.getlist(),
                mount:true
            })
    }
    async getuserInfo(){
        let test= await axios({
            method: 'get',
            url: 'http://localhost:8080/FireNation/api/getUser',
            withCredentials: true,
        });
        return test.data
        
    }
    async getUserList() {
        let test2= await axios({
            method: 'get',
            url: 'http://localhost:8080/FireNation/api/users/getAllUsers',
            withCredentials: true,
        });
        return test2.data
        /**Get user List here */
    }
    handlePost(myEvent) {
        this.setState({
            [myEvent.target.name]: myEvent.target.value
        });
    }

    handleChange(myEvent) {
        if (myEvent.target.value !== "") {
            this.setState({
                filterList: this.state.userList.filter(element => {
                    return ((element.firstName.toLowerCase() + " " + element.lastName.toLowerCase()).includes(myEvent.target.value.toLowerCase()));
                })
            });
        }
        else {
            this.setState({
                filterList: this.state.userList
            });
        }
    }

    handleImage(event) {
        if (event.target.files[0]) {
            this.setState({
                file: URL.createObjectURL(event.target.files[0]),
                fileName:event.target.files[0]
            })
            event.target.value = this.state.file;
        }
    }
    handleCancel() {

        this.setState({
            userPost: "",
            file: null,
            message: ""
        })
    }

    async handleSubmit(myEvent) {
        let pic=null;
        console.log("hhhh")
        console.log(this.state.userPost)
        if (! this.state.userPost) {
            this.setState({
                message: "Content can't be empty"
            })
            return;
        }
        if (this.state.file) {
            let key=this.state.file.split("/")[3];
           pic=s3upload(this.state.fileName, "NewUser", key);
        }
        const responsePayload = await axios({
            method: 'post',
            url: 'http://localhost:8080/FireNation/api/posts/createPost',
            data: {
           'content': this.state.userPost,
           'image': pic,
            },
            withCredentials: true,
        });
        console.log(responsePayload);
        this.setState({
            postList: await this.getlist(),
            file:null,
            userPost:""

        });
        /***Do the submit user post to databae in here  with content and pic******/
    }

    deleteImage() {
        this.setState({
            file: null
        })
    }
    handleProfile(userid) {
        this.props.history.push('/profile', { id: userid });
    }
    async getlist(){
        let getlist=await axios({
                method: 'get',
                url: 'http://localhost:8080/FireNation/api/posts/getAllPosts',
                withCredentials: true,
            });
        return getlist.data;
    }

    async rerenderParentCallback() {
        this.setState({
            postList: await this.getlist(),
        });
      }

    render() {
      
        if(!this.state.mount){
            return(
                <div>Loading</div>
            )
        }
        return (
            <div className="home">
                <div className="header">
                    <NavBarComp props={this.props}></NavBarComp>
                </div>
                <div className="usercontainer">
                    <div className="userleft">
                        <h2 className="leftText">User Information</h2>
                        <div onClick={() => this.handleProfile(this.state.userInfo.id)}>
                            <img src={this.state.userInfo.image} alt="Cinque Terre" className="userPic"></img>
                            <p className="leftText"><b>Name:</b> {this.state.userInfo.firstName} {this.state.userInfo.lastName} </p>
                        </div >
                        <p className="leftText"><b>Email:</b> {this.state.userInfo.email}</p>
                    </div>

                    <div className="usercenter">
                        <textarea className="userPost" name="userPost" rows="4" cols="50" onChange={this.handlePost} value={this.state.userPost}>
                        </textarea>
                        {(this.state.file) && <img className="postImage" src={this.state.file} alt="" />}
                        {(this.state.file) && <button className="btn btn-warning" onClick={this.deleteImage}>Delete Image</button>}
                        <br />
                        <input type="file" accept="image/x-png,image/gif,image/jpeg" onChange={this.handleImage} />
                        <button className="btn btn-success postsubmit" onClick={this.handleCancel}>
                            Cancel</button>
                        <button className="btn btn-success postsubmit" onClick={this.handleSubmit}>
                            SUBMIT</button>
                        <div className="warning">{this.state.message}</div>
                        <PostComp list={this.state.postList} rerenderParentCallback={this.rerenderParentCallback}></PostComp>
                    </div>
                    <div className="userright"><h1>User List</h1>
                        <input type="text"
                            placeholder="Search"
                            name="searchValue"
                            onChange={this.handleChange}></input>
                        <ul>
                            {this.state.filterList.map((element, index) => <li onClick={() => this.handleProfile(element.id)} key={index} >
                                {element.firstName} {element.lastName}</li>)}
                        </ul>
                    </div>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        current_id: state.valid_userId,
    }
};

export default connect(mapStateToProps)(withRouter(HomeRoute));
    //export default HomeRoute;