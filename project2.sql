DROP TABLE IF EXISTS post_comment CASCADE;
DROP TABLE IF EXISTS image CASCADE;
DROP TABLE IF EXISTS user_post CASCADE;
DROP TABLE IF EXISTS post CASCADE;
DROP TABLE IF EXISTS user_info CASCADE;
DROP TABLE IF EXISTS post_user_info CASCADE;
DROP TABLE IF EXISTS user_info_post CASCADE;
DROP TABLE IF EXISTS like_record CASCADE;
CREATE TABLE user_info(
  user_id serial PRIMARY KEY,
  username VARCHAR(25) NOT NULL UNIQUE,
  user_password VARCHAR(25) NOT NULL,
  first_name VARCHAR(25) NOT NULL,
  last_name VARCHAR(25) NOT NULL,
  email VARCHAR(250) NOT NULL UNIQUE,
  pofile_pic VARCHAR(100)
);

CREATE TABLE post(
  post_id serial PRIMARY KEY,
  user_id Integer NOT NULL,
  post_content varchar(250),
  post_time TIMESTAMP,
 FOREIGN KEY (user_id) REFERENCES user_info (user_id)
);

CREATE TABLE image(
  image_id serial PRIMARY KEY,
  post_id Integer NOT NULL,
  post_pic VARCHAR(100),
 FOREIGN KEY (post_id) REFERENCES post (post_id)
);

CREATE TABLE user_post(
	post_id Integer NOT NULL,
	user_id INTEGER NOT NULL,
	CONSTRAINT my_combo_key PRIMARY KEY (user_id, post_id),
	FOREIGN KEY (post_id) REFERENCES post (post_id),
	FOREIGN KEY (user_id) REFERENCES user_info (user_id)
);

-- Optional
CREATE TABLE post_comment(
  comment_id serial PRIMARY KEY,
  post_id Integer NOT NULL,
  user_id INTEGER NOT NULL,
comment_content VARCHAR(500),
  comment_date TIMESTAMP,	
	FOREIGN KEY (post_id) REFERENCES post (post_id),
	FOREIGN KEY (user_id) REFERENCES user_info (user_id)	

)

CREATE TABLE image_post

SELECT * FROM user_info;
SELECT * FROM post;
SELECT * FROM post_comment;
SELECT * FROM image;
SELECT * FROM user_info_post;
SELECT * FROM post_user_info;
