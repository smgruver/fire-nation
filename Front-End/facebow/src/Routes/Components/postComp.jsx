import React from 'react';
function List() {
  /** Backend: Get user first and last name, user picurer, user post content and post pic  */
  var List = [{ "user_id": 1, "firstName": "John", "lastName": "Doe", "time": "Place time here", "user_picture": "https://www.mobafire.com/images/social/build-card/draven.jpg", "user_content": "Post content test test", "post_picture": "https://www.mobafire.com/images/social/build-card/draven.jpg" },
  { "user_id": 2, "firstName": "Feng", "lastName": "Lin", "time": "Place time here2", "user_picture": "https://www.mobafire.com/images/social/build-card/draven.jpg", "user_content": "Post content test test", "post_picture": "null" },
  { "user_id": 3, "firstName": "Talon", "lastName": "wqeqweqw", "time": "Place time here3", "user_picture": "https://www.mobafire.com/images/social/build-card/draven.jpg", "user_content": "Post content test test", "post_picture": "https://www.mobafire.com/images/social/build-card/draven.jpg" }]
  return List;
}

function PostComp(props) {
  let list = List();
  console.log("test" + props.userid)
  return (
    <div>
      {list.map((element) => <div key={element.user_id} className="postComp">
        <h5 className="time">{element.time}</h5>
        <img className="post_user_image" src={element.user_picture} alt=""></img>
        <h6>{element.firstName} {element.lastName}</h6>
        <br/>
        <p className="contentP">{element.user_content}</p>
        <div>
          {(element.post_picture) && <img className="post_content_image" src={element.post_picture} alt=""></img>}
        </div>

        <button className="likeButton">Like</button>
      </div>)}

    </div>
  )
}

export default PostComp;