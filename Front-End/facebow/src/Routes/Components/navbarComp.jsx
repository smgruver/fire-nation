import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
function userLogout(props){
   props.logOut();
}
function NavBarComp(props) {
  console.log(props);
    return(
        <div className="navBarComp"> 
         <nav className="nav nav-pills nav-justified" >
        <Link className="nav-item nav-link" to="/home" style={{marginTop:"6px"}}>Home</Link>
        <Link className="nav-item nav-link" to="/settings" style={{marginTop:"6px"}}>Update userInfo</Link>
          <Link className="nav-item nav-link" to="/resetPassword" style={{marginTop:"6px"}}>Reset Password</Link>
          <Link className="nav-item nav-link" to="/"> <button className="btn btn-success" onClick={()=>userLogout(props)}>Logout</button></Link>
      </nav>
      </div>
        )
    }

    const mapDispatchToProps = dispatch => {
        return {
            logOut: () => dispatch({ type: 'LOGOUT'})
        }
    }

    

export default connect(null, mapDispatchToProps)(NavBarComp);