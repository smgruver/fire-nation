import React from 'react';
import NavBarComp from './Components/navbarComp';
import { connect } from 'react-redux';
import './../loginPages.css';

class ResetPassword extends React.Component {
    constructor(props) {
        super(props)
        if (!this.props.current_id) {
            this.props.history.push("/");
        }
        this.state = {
            oldPassword: "",
            newPassword: "",
            reNewPassword: "",
            message: ""
        }
    }

    async create(event) {
        console.log(this.state.oldPassword)
        console.log(this.state.newPassword)
        console.log(this.state.reNewPassword)
        if (!this.state.oldPassword || !this.state.newPassword || !this.state.reNewPassword) {
            this.setState({ message: "All field can't be empty" });
            return;
        }
        if (this.state.newPassword !== this.state.reNewPassword) {
            this.setState({ message: "Two new password not match" });
            return;
        }
        this.setState({ message: "success" });

    }

    oldPassword(e) {
        this.setState({ oldPassword: e.target.value });
    }

    newPassword(e) {
        this.setState({ newPassword: e.target.value });
    }

    reNewPassword(e) {
        this.setState({ reNewPassword: e.target.value });
    }

    goBack() {
        this.props.history.goBack()
    }

    //user, password
    render() {
        return (
            <React.Fragment>
                <div>
                    <NavBarComp></NavBarComp>
                    <div className="card" id='settingBody'>
                        <h1 className="titleS">Reset Password</h1>
                        <div className="setSmall">
                            Old Password:
                    </div>
                        <input type="password" className="inField" onChange={(value) => this.oldPassword(value)}></input>
                        <div className="setSmall">
                            New Password:
                    </div>
                        <input type="password" className="inField" onChange={(value) => this.newPassword(value)} ></input>
                        <div className="setSmall">
                            Retype password:
                    </div>
                        <input type="password" className="inField" onChange={(value) => this.reNewPassword(value)} ></input>

                        <div className="warnMessage titleS">
                            {this.state.message}
                        </div>
                        <button type="button" className="loginButton" onClick={() => this.create()}>Update Settings</button>

                        <button className="btn backButton" onClick={() => this.goBack()}>Back</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

const mapStateToProps = state => {
    return {
        current_id: state.valid_userId
    }
};

export default connect(mapStateToProps)(ResetPassword);