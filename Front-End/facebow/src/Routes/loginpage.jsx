import React from 'react';
import './../loginPages.css';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';

class LoginPage extends React.Component {
    constructor(props) {
        super(props)
        if (this.props.current_id) {
            this.props.history.push("/home");
        }
        this.state = {
            loginId: "",
            passwordId: "",
            warnMessage: ""
        }
    }

    checkLogin(event) {
        console.log(this.state.loginId);
        console.log(this.state.passwordId);

        if (!this.state.loginId || !this.state.passwordId) {
            this.setState({ warnMessage: "Please fill in the username and password" });
            return;
        }
        /*********Connect to the backend in here********** */
        this.props.logIn(1);
        this.props.history.push('/home');
        //console.log(this.props.current_id)

    }


    updateLogin(e) {
        this.setState({ loginId: e.target.value });
    }

    updatePass(e) {
        this.setState({ passwordId: e.target.value });
    }

    render() {
        return (
            <React.Fragment>
                <div className="card" id='loginCard'>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <h1 className="titleW">Login</h1>
                    <div className="loginSmall">
                        Name:
                    </div>
                    <input type="username" className="inField" id="usernameId" onChange={(value) => this.updateLogin(value)} value={this.state.loginId}></input>

                    <div className="loginSmall">
                        Password:
                    </div>
                    <input type="password" className="inField" id="passwordId" onChange={(value) => this.updatePass(value)} value={this.state.passwordId}></input>
                    <button type="button" className="loginButton" onClick={() => this.checkLogin()}>Login</button>
                    <div className="warnMessage">{this.state.warnMessage}</div>
                    <Link to="/createUser">
                        <button className="createUse">Create User</button>
                    </Link>
                    <Link to="/resetPass">
                        <button className="resetPassBtn">Reset Password</button>
                    </Link>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                </div>
            </React.Fragment>
        )
    }

}
const mapDispatchToProps = dispatch => {
    return {
        logIn: (name) => dispatch({ type: 'LOGIN', user: name }),
        logOut: () => dispatch({ type: 'LOGOUT' })
    }
}

const mapStateToProps = state => {
    return {
        current_id: state.valid_userId,
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LoginPage));;