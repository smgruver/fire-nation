import React from 'react';
import NavBarComp from './Components/navbarComp';
import PostComp from './Components/postComp';
import { connect } from 'react-redux';
class ProfileRoute extends React.Component {
   constructor(props) {
      super(props);

      if(!this.props.current_id){
         this.props.history.push("/");
     }
      this.state = { 
         userInfo: []
      }
      this.getUserInfo();
   }

   getUserInfo(){
      /** Get user information in here */
      this.state.userInfo.push({"userid":1, "firstName":"John", "lastName":"Doe", "email":"test@gmail.come", "picture":"https://www.mobafire.com/images/social/build-card/draven.jpg"});
  }

   render() {
    return (
      <div className="home">
      <div className="header">
         <NavBarComp props={this.props}></NavBarComp>
      </div>   
      <div className="profileUsercontainer">
         <div className="profileUserleft">
        <h2>User Information</h2> 
        <div onClick={() => this.handleProfile(this.state.userInfo[0].userid)}>
        <img src={this.state.userInfo[0].picture} alt="Cinque Terre" className="profileUserPic"></img>
        <p><b>Name:</b> {this.state.userInfo[0].firstName} {this.state.userInfo[0].lastName} </p>
        </div >
        <p><b>Email:</b> {this.state.userInfo[0].email}</p>
        </div>
        <div className="postUsercenter">
         <PostComp userid={this.props.location.state.id}></PostComp>
        </div>
    
   
      </div>
        
    </div>
    )
    }
}

const mapStateToProps = state => {
   return {
       current_id: state.valid_userId,
   }
}; 

    
    export default  connect(mapStateToProps)(ProfileRoute);