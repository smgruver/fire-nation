import React from 'react';
import NavBarComp from './Components/navbarComp';
import { connect } from 'react-redux';
import './../loginPages.css';
import { s3upload } from './bucket';

class SettingPage extends React.Component {
    constructor(props) {
        super(props)
        if (!this.props.current_id) {
            this.props.history.push("/");
        }

        this.state = {

            message: "",
            username: "",
            password: "",
            firstname: "",
            lastname: "",
            email: "",
            userInfo: this.getUserInfo(),
            file: this.getUserInfo().image
        }
        this.handleImage = this.handleImage.bind(this);
    }

    getUserInfo() {
        /** Get user information in here */
        return { "userName": "fxl2968", "firstName": "John", "lastName": "Doe", "email": "test@gmail.come", "image": "https://www.mobafire.com/images/social/build-card/draven.jpg" };
    }

    async create(event) {
        //Checks if valid(with backend)
        if (this.state.file !== this.state.userInfo.image) {
            let key = this.state.file.split("/")[3];
            //Implement S3 in here
            console.log(key); //identify key
            console.log(this.state.file); // Blob file
        }
        let state = this.tryCreating();
        if (state) {
            this.setState({ message: "User created!" });
        } else {
            this.setState({ message: "Username or email taken!" });
        }

    }
    handleImage(event) {
        if (event.target.files[0]) {
            this.setState({
                file: URL.createObjectURL(event.target.files[0])
            })
            event.target.value = null;
        }
    }
    //axios request that returns true if user created otherwise false
    async tryCreating() {
        return true;

    }

    updateUser(e) {
        this.setState({ username: e.target.value });
    }

    updatePass(e) {
        this.setState({ password: e.target.value });
    }

    updateFirst(e) {
        this.setState({ firstname: e.target.value });
    }

    updateLast(e) {
        this.setState({ lastname: e.target.value });
    }

    updateEm(e) {
        this.setState({ email: e.target.value });
    }

    goBack() {
        this.props.history.goBack()
    }

    //user, password
    render() {
        return (
            <React.Fragment>
                <div>
                    <NavBarComp></NavBarComp>
                    <div className="card" id='settingBody'>
                        <h1 className="titleS">Settings</h1>
                        <img className="postImage" src={this.state.file} alt="" />

                        <br />
                        <input type="file" className="titleS" id="test" accept="image/x-png,image/gif,image/jpeg" onChange={this.handleImage} />

                        <div className="setSmall">
                            Username:
                        </div>
                        <input type="username" className="inField" id="usernameC" onChange={(value) => this.updateUser(value)} value={this.state.userInfo.userName}></input>
                        <div className="setSmall">
                            First name:
                        </div>
                        <input type="username" className="inField" id="firstNC" onChange={(value) => this.updateFirst(value)} value={this.state.userInfo.firstName}></input>
                        <div className="setSmall">
                            Last Name:
                        </div>
                        <input type="username" className="inField" id="lastNC" onChange={(value) => this.updateLast(value)} value={this.state.userInfo.lastName}></input>
                        <div className="setSmall">
                            Email:
                        </div>
                        <input type="email" className="inField" id="emailC" onChange={(value) => this.updateEm(value)} value={this.state.userInfo.email}></input>

                        <div className="warnMessage titleS">
                            {this.state.message}
                        </div>
                        <button type="button" className="loginButton" onClick={() => this.create()}>Update Settings</button>

                        <button className="btn backButton" onClick={() => this.goBack()}>Back</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }

}

const mapStateToProps = state => {
    return {
        current_id: state.valid_userId
    }
};

export default connect(mapStateToProps)(SettingPage);