import React from 'react';
import NavBarComp from './Components/navbarComp';
import './../loginPages.css';
import PostComp from './Components/postComp';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { s3upload } from './bucket';
class HomeRoute extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userPost: "",
            file: null,
            searchValue: "",
            userInfo: this.getUserInfo(),
            userList: this.getUserList(),
            filterList: this.getUserList()
        };

        if (!this.props.current_id) {
            this.props.history.push("/");
        }

        this.handlePost = this.handlePost.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleImage = this.handleImage.bind(this);
        this.handleProfile = this.handleProfile.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.deleteImage = this.deleteImage.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.getUserInfo()
    }
    getUserList() {

        /**Get user List here */
        return [{ "userid": 1, "firstName": "Talon", "lastName": "Lin" },
        { "userid": 2, "firstName": "Riven", "lastName": "Lian" },
        { "userid": 3, "firstName": "test", "lastName": "shun" },
        { "userid": 4, "firstName": "zed", "lastName": "ten" },
        { "userid": 5, "firstName": "ten", "lastName": "night" },
        { "userid": 6, "firstName": "night", "lastName": "test" },
        { "userid": 7, "firstName": "John6", "lastName": "ted" }];
    }
    getUserInfo() {
        /**Get user information in here */
        return { "userid": 1, "firstName": "John", "lastName": "Doe", "email": "test@gmail.come", "picture": "https://www.mobafire.com/images/social/build-card/draven.jpg" };
    }
    handlePost(myEvent) {
        this.setState({
            [myEvent.target.name]: myEvent.target.value
        });
    }

    handleChange(myEvent) {
        console.log(myEvent.target.value);
        if (myEvent.target.value !== "") {
            this.setState({
                filterList: this.state.userList.filter(element => {
                    return ((element.firstName.toLowerCase() + " " + element.lastName.toLowerCase()).includes(myEvent.target.value.toLowerCase()));
                })
            });
        }
        else {
            this.setState({
                filterList: this.state.userList
            });
        }
    }

    handleImage(event) {
        if (event.target.files[0]) {
            this.setState({
                file: URL.createObjectURL(event.target.files[0])
            })
            // event.target.value = null;
            console.log(event.target.files[0])
            let x = s3upload(event.target.files[0],this.state.userInfo.userid, "testCase")
            console.log("URL: "+x)
        }
    }
    handleCancel() {

        this.setState({
            userPost: "",
            file: null,
            message: ""
        })
    }

    handleSubmit(myEvent) {

        if (!this.state.file && !this.state.userPost) {
            this.setState({
                message: "Content or image must fill in one for post"
            })
            console.log(this.state.message);
            return;
        }

        if (this.state.file) {
            let key = this.state.file.split("/")[3];
            //Implement S3 in here
            console.log(key); //identify key
            console.log(this.state.file); // Blob file
        }

        console.log(this.state.userPost);
        console.log(this.state.file);
        /***Do the submit user post to databae in here  with content and pic******/
    }

    deleteImage() {
        this.setState({
            file: null
        })
    }
    handleProfile(userid) {
        //console.log("test");
        // console.log(userid);
        this.props.history.push('/profile', { id: userid });
    }

    render() {
        console.log(1)
        return (
            <div className="home">
                <div className="header">
                    <NavBarComp props={this.props}></NavBarComp>
                </div>
                <div className="usercontainer">
                    <div className="userleft">
                        <h2 className="leftText">User Information</h2>
                        <div onClick={() => this.handleProfile(this.state.userInfo.userid)}>
                            <img src={this.state.userInfo.picture} alt="Cinque Terre" className="userPic"></img>
                            <p className="leftText"><b>Name:</b> {this.state.userInfo.firstName} {this.state.userInfo.lastName} </p>
                        </div >
                        <p className="leftText"><b>Email:</b> {this.state.userInfo.email}</p>
                    </div>

                    <div className="usercenter">
                        <textarea className="userPost" name="userPost" rows="4" cols="50" onChange={this.handlePost} value={this.state.userPost}>
                        </textarea>
                        {(this.state.file) && <img className="postImage" src={this.state.file} alt="" />}
                        {(this.state.file) && <button className="btn btn-warning" onClick={this.deleteImage}>Delete Image</button>}
                        <br />
                        <input type="file" accept="image/x-png,image/gif,image/jpeg" onChange={this.handleImage} />
                        <button className="btn btn-success postsubmit" onClick={this.handleCancel}>
                            Cancel</button>
                        <button className="btn btn-success postsubmit" onClick={this.handleSubmit}>
                            SUBMIT</button>
                        <div className="warnMessage titleS">{this.state.message}</div>
                        <PostComp userid="-1"></PostComp>
                    </div>
                    <div className="userright"><h1>User List</h1>
                        <input type="text"
                            placeholder="Search"
                            name="searchValue"
                            onChange={this.handleChange}></input>
                        <ul>
                            {this.state.filterList.map((element, index) => <li onClick={() => this.handleProfile(element.userid)} key={index} >
                                {element.firstName} {element.lastName}</li>)}
                        </ul>
                    </div>
                </div>

            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        current_id: state.valid_userId,
    }
};

export default connect(mapStateToProps)(withRouter(HomeRoute));
    //export default HomeRoute;