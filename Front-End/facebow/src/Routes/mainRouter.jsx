import React from 'react';
import LoginRoute from './loginpage';
import HomeRoute from './homepage'
import ProfileRoute from './profilepage';
import SettingsRoute from './settingspage';
import ResetPassword from './resetPassword'
import { BrowserRouter, Route} from 'react-router-dom'
import PassRe from './resetPass';
import CreateUser from './createUser';

function ReactRouter() {
    return (
        <React.Fragment>
            <BrowserRouter>
                <Route exact path='/' component={LoginRoute}></Route>
                <Route path='/home' component={HomeRoute}></Route>
                <Route path='/profile' component={ProfileRoute}></Route>
                <Route path='/settings' component={SettingsRoute}></Route>
                <Route path='/resetPass' component={PassRe}></Route>
                <Route path='/resetPassword' component={ResetPassword}></Route>
                <Route path='/createUser' component={CreateUser}></Route>
            </BrowserRouter>
        </React.Fragment>
    )
}

export default ReactRouter;