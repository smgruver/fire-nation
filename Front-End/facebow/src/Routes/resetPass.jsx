import React from 'react';
import './../loginPages.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
class PassRe extends React.Component {
    constructor(props) {
        super(props)
        if (this.props.current_id) {
            this.props.history.push("/home");
        }
        this.state = {
            message: "",
            email: ""
        }
    }

    async sendEmail(event) {
        if (!this.state.email) {
            this.setState({ message: "Email can't be empty" });
            return;
        }
        //Checks if valid(with backend)
        this.setState({ message: "Email set to reset password" });
    }

    updateEm(e) {
        this.setState({ email: e.target.value });
    }

    render() {
        return (
            <React.Fragment>
                <div className="card" id='loginCard'>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <h1 className="titleW">Reset Password</h1>
                    <div className="loginSmall">
                        Email:
                    </div>
                    <input type="email" className="inField" id="usernameId" onChange={(value) => this.updateEm(value)} value={this.state.email}></input>

                    <div className="warnMessage titleS">
                        {this.state.message}
                    </div>
                    <button type="button" className="loginButton" onClick={() => this.sendEmail()}>Reset</button>
                    <Link to="/">
                        <button className="btn btn-danger">Back</button>
                    </Link>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                </div>
            </React.Fragment>
        )
    }

}
const mapStateToProps = state => {
    return {
        current_id: state.valid_userId,
    }
};
export default connect(mapStateToProps)(PassRe);