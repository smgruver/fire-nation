
const initialState = {
    authenticated: false,
    username: ""
}
function saveToLocalStorage(state){
    try{
        const serializedState=JSON.stringify(state);
        localStorage.setItem('state', serializedState)
    }catch(e){
        console.log(e)
    }
}
function loadFromLocalStorage(){
    try{
        const serializedState= localStorage.getItem('state')
        if(serializedState===null) return "";
        return  JSON.parse(serializedState);
    }catch(e){
        return ""
    }
}
//Reducer
const reducer = (state = { ...initialState }, action) => {
    switch (action.type) {
        case "LOGIN":
            console.log("loggg");
            saveToLocalStorage(action.user);
            //return state object but with logged in stuff to get username use action.username
            return {
                valid_userId: action.user
            }
        case "LOGOUT":
            console.log("logout");
            localStorage.clear();
            //return state object but with default/logged out in stuff
            return {
                valid_userId: ""
            }
        default:
            return {
                valid_userId: loadFromLocalStorage()
            }
    }
}

export default reducer;