import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from './Routes/Redux/Reducer';
import { BrowserRouter } from 'react-router-dom';
//Routes
import LoginRoute from './Routes/loginpage';
import CreateUser from './Routes/createUser';
import HomeRoute from './Routes/homepage'
import ProfileRoute from './Routes/profilepage';
import SettingsRoute from './Routes/settingspage';
import PassRe from './Routes/resetPass';

Enzyme.configure({ adapter: new Adapter() });

const store = createStore(reducer)


test('Testing Login', () => {
  let wrapper
  wrapper = mount(<Provider store={store}><BrowserRouter><LoginRoute/></BrowserRouter></Provider>);
  
  // this.props.logIn("userTest")
  // console.log(this.props.current_id);
  let wrapperInst = wrapper.instance();

  //Instances
  expect(wrapperInst).toBeFalsy();

  //Card exists
  let text = wrapper.find('#loginCard');
  expect(text).not.toBeUndefined();

  //Tags
  const text1 = wrapper.find('br');
  expect(text1).not.toBeUndefined();

  //Headers
  const text2 = wrapper.find('.titleW');
  expect(text2.text()).toBe("Login");
  const text3 = wrapper.find('.loginSmall');
  expect(text3.at(0).text()).toBe("Name:");
  expect(text3.at(1).text()).toBe("Password:");
  const text4 = wrapper.find('.warnMessage');
  expect(text4.text()).toBe("");

  //Buttons
  const btn1 = wrapper.find('.inField');
  expect(btn1.first().text()).toBe("");
  expect(btn1.at(1).text()).toBe("");
  const btn2 = wrapper.find('.createUse');
  expect(btn2.text()).toBe("Create User");
  const btn3 = wrapper.find('.resetPassBtn');
  expect(btn3.text()).toBe("Reset Password");
  const btn4 = wrapper.find('.loginButton');
  expect(btn4.first().text()).toBe("Login");
});

test('Testing create User', () => {
  let wrapper
  wrapper = mount(<Provider store={store}><BrowserRouter><CreateUser/></BrowserRouter></Provider>);

  let wrapperInst = wrapper.instance();

  //Instances
  expect(wrapperInst).toBeFalsy();

  //Card exists
  let text = wrapper.find('#newACard');
  expect(text).not.toBeUndefined();

  //Tags
  const text1 = wrapper.find('br');
  expect(text1).not.toBeUndefined();

  //Headers
  const text2 = wrapper.find('.titleW');
  expect(text2.text()).toBe("New Account");
  const text3 = wrapper.find('.loginSmall');
  expect(text3.at(0).text()).toBe("Username:");
  expect(text3.at(1).text()).toBe("Password:");
  expect(text3.at(2).text()).toBe("Comfirmed Password:");
  expect(text3.at(3).text()).toBe("First name:");
  expect(text3.at(4).text()).toBe("Last Name:");
  expect(text3.at(5).text()).toBe("Email:");
  const text4 = wrapper.find('.warnMessage');
  expect(text4.text()).toBe("");

  //Buttons
  const btn1 = wrapper.find('.inField');
  expect(btn1.first().text()).toBe("");
  expect(btn1.at(1).text()).toBe("");
  expect(btn1.at(2).text()).toBe("");
  expect(btn1.at(3).text()).toBe("");
  expect(btn1.at(4).text()).toBe("");
  expect(btn1.at(5).text()).toBe("");
  const btn2 = wrapper.find('.btn');
  expect(btn2.first().text()).toBe("Back");
  const btn4 = wrapper.find('.loginButton');
  expect(btn4.first().text()).toBe("Create User");
})

test('Testing Reset Password', () => {
  let wrapper
  wrapper = mount(<Provider store={store}><BrowserRouter><PassRe/></BrowserRouter></Provider>);

   // console.log(this.props.current_id);
   let wrapperInst = wrapper.instance();

   //Instances
   expect(wrapperInst).toBeFalsy();
 
   //Card exists
   let text = wrapper.find('#loginCard');
   expect(text).not.toBeUndefined();
 
   //Tags
   const text1 = wrapper.find('br');
   expect(text1).not.toBeUndefined();
 
   //Headers
   const text2 = wrapper.find('.titleW');
   expect(text2.text()).toBe("Reset Password");
   const text3 = wrapper.find('.loginSmall');
   expect(text3.text()).toBe("Email:");
   const text4 = wrapper.find('.warnMessage');
   expect(text4.text()).toBe("");
 
   //Buttons
   const btn1 = wrapper.find('.inField');
   expect(btn1.text()).toBe("");
   const btn3 = wrapper.find('.btn');
   expect(btn3.text()).toBe("Back");
   const btn4 = wrapper.find('.loginButton');
   expect(btn4.first().text()).toBe("Reset");
})

test('Testing Login', () => {
  let wrapper
  wrapper = mount(<Provider store={store}><BrowserRouter><LoginRoute props={{authenticated: true,
    username: "1"}}/></BrowserRouter></Provider>);

    let text = wrapper.find('div');
   expect(text).not.toBeUndefined();
})