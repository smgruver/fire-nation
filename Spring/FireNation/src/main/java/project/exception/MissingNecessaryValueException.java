package project.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="A necessary value for your request was found null.")
public class MissingNecessaryValueException extends RuntimeException {

	public MissingNecessaryValueException() {
		// TODO Auto-generated constructor stub
	}

	public MissingNecessaryValueException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MissingNecessaryValueException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MissingNecessaryValueException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MissingNecessaryValueException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
