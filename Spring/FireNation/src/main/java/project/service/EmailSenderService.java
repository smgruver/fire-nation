package project.service;

import java.security.SecureRandom;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Contains static methods necessary for sending email to users.
 * Used primarily for sending emails when passwords are to be reset to a random string.
 * @author sgruv
 *
 */
public class EmailSenderService {
	public static void main(String[] args) throws MessagingException {
		EmailSenderService.sendMail("icorrea81993@gmail.com", "Reset your password");
	}

	/**
	 * Sends an email to 'recipient' containting 'messageContent' via an SMTP server.
	 * @param recipient
	 * @param messageContent
	 * @throws MessagingException
	 */
	public static void sendMail(String recipient, String messageContent) throws MessagingException {
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.startls.enable", "true");
		properties.put("mail.smtp.host", "smtp.sendgrid.net");
		properties.put("mail.smtp.port", "587");

		String username = "apikey";
		String myAccountEmail = "icorrea81993@gmail.com";
		String password = "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";

		Session session = Session.getInstance(properties, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				// TODO Auto-generated method stub
				return new PasswordAuthentication(username, password);
			}
		});

		Message message = prepareMessage(session, myAccountEmail, recipient, messageContent);

		Transport.send(message);
		System.out.println("Message sent successfully");
	}

	/**
	 * Generates a random 10-character alphanumeric String to replace the user's current password.
	 * @param len
	 * @return
	 */
	public static String generateRandomPassword(int len) {
		// ASCII range - alphanumeric (0-9, a-z, A-Z)
		final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		SecureRandom random = new SecureRandom();
		StringBuilder sb = new StringBuilder();

		// each iteration of loop choose a character randomly from the given ASCII range
		// and append it to StringBuilder instance

		for (int i = 0; i < len; i++) {
			int randomIndex = random.nextInt(chars.length());
			sb.append(chars.charAt(randomIndex));
		}

		return sb.toString();
	}

	/**
	 * Assembles the {@link Message} object to serve as the email sent to the recipient.
	 * @param session
	 * @param fromEmail
	 * @param recipient
	 * @param messageContent
	 * @return
	 */
	private static Message prepareMessage(Session session, String fromEmail, String recipient, String messageContent) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
			message.setSubject("Reset your password");
			message.setText(messageContent);
			return message;
		} catch (Exception e) {
			Logger.getLogger(EmailSenderService.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}
}
