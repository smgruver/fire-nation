package project.service;

import java.util.List;

import project.model.JsonPostInput;
import project.model.Post;
import project.model.User;

public interface PostService {

	public void addPost(Post myPost);
	
	public List<Post> getAllPosts();
	public Post getPostbyId(int id);
	public List<Post> getPostsByAuthorId(int myAuthorId);
//	public List<Post> getLikes(int likers);		//unnecessary; if we want the users who liked a post we just need to get the post and use the getLikers() method
	
	public void editPost(JsonPostInput myPost);
	public boolean likeButton(User liker, Post targetPost);
	
	public void removePost(Post myPost);
}
