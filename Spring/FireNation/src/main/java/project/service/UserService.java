package project.service;

import java.util.List;

import project.model.EditUserBody;
import project.model.EditUserPasswordRequestBody;
import project.model.SessionUser;
import project.model.User;
import project.model.UserPage;

public interface UserService {

	public void addUser(User myUser);
	
	public List<User> getAllUsers();
	public User getUserById(int id);
	public User getUserByUsername(String username);
	public User getUserByEmail(String email);
	public UserPage getUserPageById(int userId);
	
	public User editUser(User currentUser, EditUserBody myUser);
	public boolean editUserPassword(User currentUser, EditUserPasswordRequestBody myUser);
	public String resetPassword(User myUser);
	public boolean editUserPasswordNoHash(User currentUser, EditUserPasswordRequestBody myPasswords);
	
	public void removeUser(User myUser);	
	
	public User loginUser(SessionUser loginAttempt);
}
