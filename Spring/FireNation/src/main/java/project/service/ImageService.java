package project.service;

import java.util.List;

import project.model.Image;

public interface ImageService {

	public void addImage(Image myImage);
	
	public List<Image> getAllImages();
	public Image getImageById(int id);
	public List<Image> getImagesByPostId(int id);
	
	public void editImage(Image myImage);
	
	public void removeImage(Image myImage);	
}
