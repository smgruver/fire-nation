package project.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.dao.CommentRepo;
import project.model.Comment;
import project.model.Post;

@Service("commentServ")
public class CommentServiceImpl implements CommentService {

	private CommentRepo commentRepo;

	public CommentServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public CommentServiceImpl(CommentRepo commentRepo) {
		this.commentRepo = commentRepo;
	}
	
	@Override
	public void addComment(Comment myComment) {
		myComment.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
		commentRepo.insert(myComment);	
	}

	@Override
	public Comment getCommentById(int id) {
		return commentRepo.selectById(id);
		
	}

	@Override
	public List<Comment> getAllComments() {
		return commentRepo.selectAll();
	}

	@Override
	public List<Comment> getCommentsByPostId(int postId) {
		return commentRepo.selectByPostId(postId);
	}
	
	@Override
	public List<Comment> getCommentsByAuthorId(int authorId) {
		return commentRepo.selectByAuthorId(authorId);
	}
	
	@Override
	public void editComment(Comment myComment) {
		Comment oldComment = commentRepo.selectById(myComment.getId());
		oldComment.setContent(myComment.getContent());
		myComment = oldComment;
		commentRepo.update(myComment);
	}

	@Override
	public void removeComment(Comment myComment) {
		commentRepo.delete(myComment);
	}
}
