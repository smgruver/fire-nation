package project.service;

import java.security.SecureRandom;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import project.controller.UserController;
import project.dao.PostRepo;
import project.dao.UserRepo;
import project.exception.NonUniqueValueException;
import project.model.EditUserBody;
import project.model.EditUserPasswordRequestBody;
import project.model.Post;
import project.model.SessionUser;
import project.model.User;
import project.model.UserPage;

/**
 * Contains methods called by {@link UserController} to perform business logic
 * and interface with {@link UserRepo}.
 * 
 * @author sgruv
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	private UserRepo userRepo;
	private PostRepo postRepo;

	int bCryptStrength = 10; // determines bCrypt's work-factor
	private BCryptPasswordEncoder bCryptEncoder = new BCryptPasswordEncoder(bCryptStrength, new SecureRandom()); // used to
																											// hash
																											// passwords

	/**
	 * No args constructor used by Spring to set up for autowiring.
	 */
	public UserServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Accepts a {@link UserRepo} and {@link PostRepo}, expected to be Spring Beans.
	 * 
	 * @param userRepo
	 * @param postRepo
	 */
	@Autowired
	public UserServiceImpl(UserRepo userRepo, PostRepo postRepo) {
		this.userRepo = userRepo;
		this.postRepo = postRepo;
	}

	/**
	 * Accepts a {@link User} object and passes it to {@link UserRepo}'s insert
	 * method to be added to the database.
	 * 
	 * @param myUser the User to be added to the database
	 * @return void
	 */
	@Override
	public void addUser(User myUser) {
		System.out.println(myUser);
		
		String hashedPassword = bCryptEncoder.encode(myUser.getPassword());
		myUser.setPassword(hashedPassword);
		
		userRepo.insert(myUser);
	}

	/**
	 * Uses {@link UserRepo}'s selectAll method to return every {@link User} object
	 * from the database in a {@link List}
	 * 
	 * @return List<User>
	 */
	@Override
	public List<User> getAllUsers() {
		return userRepo.selectAll();
	}

	/**
	 * Accepts an int 'id' to find a {@link User} object from the database with a
	 * matching user_id field via {@link UserRepo}'s selectById method.
	 * 
	 * @param id the id of the User to be found
	 * @Return User
	 */
	@Override
	public User getUserById(int id) {
		return userRepo.selectById(id);
	}

	/**
	 * Accepts an String 'username' to find a {@link User} object from the database
	 * with a matching username field via {@link UserRepo}'s selectByUsername
	 * method.
	 * 
	 * @param username the username of the User to be found
	 * @return User
	 */
	@Override
	public User getUserByUsername(String username) {
		return userRepo.selectByUsername(username);
	}

	/**
	 * Accepts an String 'email' to find a {@link User} object from the database
	 * with a matching email field via {@link UserRepo}'s selectByEmail method.
	 * 
	 * @param the email of the user to be found
	 * @return User
	 */
	@Override
	public User getUserByEmail(String email) {
		User target = userRepo.selectByEmail(email);
		return target;
	}

	/**
	 * Accepts an int 'userId' to gather the information necessary to form the
	 * {@link UserPage} for the {@link User} object that corresponds with the given
	 * id.
	 * 
	 * @param userId the id of the User whose info is to be returned
	 * @return UserPage
	 */
	@Override
	public UserPage getUserPageById(int userId) {
		User targetUser = userRepo.selectById(userId);
		List<Post> userPosts = postRepo.selectByAuthorId(userId);
//		List<Post> userLikes = postRepo.selectByLikerId(userId);

		return new UserPage(targetUser, userPosts);
//		return new UserPage(targetUser, userPosts, userLikes);
	}

	/**
	 * Accepts an {@link EditUserBody} object and a {@link User} object to replace
	 * mutable information in the User with information from the EditUserBody. If
	 * any fields in the EditUserBody are null, the corresponding field in the User
	 * will retain its original value.
	 * 
	 * @param currentUser the User whose field values are to be replaced
	 * @param myUser      the EditUserBody whose field values are to replace those
	 *                    in currentUser
	 * @return User the changed User
	 */
	@Override
	public User editUser(User currentUser, EditUserBody myUser) {
		
		User sameUsername = userRepo.selectByUsername(currentUser.getUsername());
		User sameEmail = userRepo.selectByEmail(currentUser.getEmail());
		
		boolean usernameTaken = (sameUsername != null && !sameUsername.equals(currentUser));
		boolean emailTaken = (sameEmail != null && !sameEmail.equals(currentUser));

		if (usernameTaken && emailTaken) {
			throw new NonUniqueValueException("Both username and email are already taken!");
		} else if (usernameTaken) {
			throw new NonUniqueValueException("That username is already taken!");
		} else if (emailTaken) {
			throw new NonUniqueValueException("That email is already taken!");
		}

		currentUser.setUsername((myUser.getUsername() != null) ? myUser.getUsername() : currentUser.getUsername());
		currentUser.setFirstName((myUser.getFirstName() != null) ? myUser.getFirstName() : currentUser.getFirstName());
		currentUser.setLastName((myUser.getLastName() != null) ? myUser.getLastName() : currentUser.getLastName());
		currentUser.setEmail((myUser.getEmail() != null) ? myUser.getEmail() : currentUser.getEmail());

		userRepo.update(currentUser);
		
		return currentUser;
	}

	/**
	 * Checks if the current User's password matches myPasswords's oldPassword field
	 * and sets current User's password to myPasswords's newPassword field if it
	 * does. Includes hashing of myPasswords's fields when comparing and setting.
	 */
	@Override
	public boolean editUserPassword(User currentUser, EditUserPasswordRequestBody myPasswords) {
		//String hashedOld = bCrypt.encode(myPasswords.getOldPassword());

		//if (currentUser.getPassword() == hashedOld) {
		if (BCrypt.checkpw(myPasswords.getOldPassword(), currentUser.getPassword())) {
			String hashedNew = bCryptEncoder.encode(myPasswords.getNewPassword());
			currentUser.setPassword(hashedNew);
			userRepo.update(currentUser);
			return true;
		} else {
			return false;
		}

	}
	
	@Override
	public boolean editUserPasswordNoHash(User currentUser, EditUserPasswordRequestBody myPasswords) {
		String hashedOld = bCryptEncoder.encode(myPasswords.getOldPassword());

		if (currentUser.getPassword() == hashedOld) {
//		if (BCrypt.checkpw(myPasswords.getOldPassword(), currentUser.getPassword())) {
//			String hashedNew = bCryptEncoder.encode(myPasswords.getNewPassword());
//			currentUser.setPassword(hashedNew);
			currentUser.setPassword(myPasswords.getNewPassword());
			userRepo.update(currentUser);
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Accepts a {@link User} object and removes the corresponding record from the
	 * database
	 * 
	 * @param myuser the User to be removed from the database
	 * @return void
	 */
	@Override
	public void removeUser(User myUser) {
		userRepo.delete(myUser);
	}

	/**
	 * Accepts a {@link SessionUser} object and compares its username and password
	 * fields to {@link User} records int he database for login verification.
	 * Returns the corresponding User if a match was found.
	 * 
	 * @param loginAttempt the SessionUser whose field values will be compared to
	 *                     User records for login validation
	 * @return User
	 */
	@Override
	public User loginUser(SessionUser loginAttempt) {
		User target = userRepo.selectByUsername(loginAttempt.getUsername());

//		if (target == null || !loginAttempt.getPassword().equals(target.getPassword())) {
		if (target == null || !BCrypt.checkpw(loginAttempt.getPassword(), target.getPassword())) {
			return null;
		} else {
			return target;
		}
	}

	/**
	 * Accepts a {@link User} object, replaces that User's email field with a random
	 * String, and sends an email with that String to the User's email address. The
	 * random String will be alphanumeric and be 10 characters long. The email is
	 * sent to the User's address via SMTP server.
	 */
	@Override
	public String resetPassword(User myUser) {
		System.out.println(myUser.getId() + " " + myUser.getUsername());
		
		
		// GENERATE RANDOM PASSWORD
		String randomPassword = EmailSenderService.generateRandomPassword(10);
		
		myUser.setPassword(randomPassword);
		// GENERATE RANDOM PASSWORD
		String unhashedRandom = EmailSenderService.generateRandomPassword(10);
		String hashedRandom = bCryptEncoder.encode(unhashedRandom);

		myUser.setPassword(hashedRandom);
		userRepo.update(myUser);

		return unhashedRandom;
	}

}
