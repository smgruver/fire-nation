package project.service;

import java.util.List;

import project.model.Comment;

public interface CommentService {

	public void addComment(Comment myComment);
	
	public List<Comment> getAllComments();
	public List<Comment> getCommentsByPostId(int postId);
	public List<Comment> getCommentsByAuthorId(int authorId);
	public Comment getCommentById(int id);
	
	public void editComment(Comment myComment);
	
	public void removeComment(Comment myComment);
}
