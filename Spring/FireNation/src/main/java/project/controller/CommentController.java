package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import project.model.Comment;
import project.service.CommentService;

//@RequestMapping("/comments")
//@Controller
//@CrossOrigin(origins="*")
public class CommentController {

	//FIELDS
	private CommentService commentServ;
	
	//CONSTRUCTORS
	public CommentController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public CommentController(CommentService commentServ) {
		super();
		this.commentServ = commentServ;
	}
	
	//METHODS	
	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping(value = "/getAllComments")
	public @ResponseBody List<Comment> getAllcomments(){
		return commentServ.getAllComments();
	}
	
	@GetMapping(value = "/getCommentById")
	public @ResponseBody Comment getcommentById(@RequestParam("id") int num) {
		return commentServ.getCommentById(num);
	}
	
	@GetMapping(value = "/getCommentsByAuthor")
	public @ResponseBody List<Comment> getcommentsByAuthor(@RequestParam("author") int authorId) {
		return commentServ.getCommentsByAuthorId(authorId);
	}
	
	@GetMapping(value = "/getCommentsByPost")
	public @ResponseBody List<Comment> getcommentsByPost(@RequestParam("post") int postId) {
		return commentServ.getCommentsByPostId(postId);
	}
	
	@PutMapping(value = "/editCommentContent")
	public @ResponseBody String editcomment(@RequestBody Comment incomingComment) {
		
		commentServ.editComment(incomingComment);
		return "Success";
	}
	
	@PostMapping(value = "/createComment")
	public @ResponseBody String createNewcomment(@RequestBody Comment incomingComment) {
		commentServ.addComment(incomingComment);
		return "Success";
	}
	
	@DeleteMapping(value = "/deleteComment")
	public @ResponseBody String deletecomment(@RequestBody Comment incomingComment) {
		commentServ.removeComment(incomingComment);
		return "Success";
	}
	
	@DeleteMapping(value = "/deleteCommentById")
	public @ResponseBody String deleteUser(@RequestBody int commentId) {
		commentServ.removeComment(commentServ.getCommentById(commentId));
		return "Success";
	}
	
	
}
