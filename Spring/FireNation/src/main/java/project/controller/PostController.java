
package project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import project.model.JsonPostInput;
import project.model.JsonSingleInput;
import project.model.Post;
import project.model.PostPage;
import project.model.SessionUser;
import project.model.User;
import project.service.PostService;
import project.service.UserService;

/**
 * Controller Component for Sping to accept and handle User-oriented requests
 * made to the API. Endpoints in this controller have a prefix of "/users"
 * 
 * @author sgruv
 *
 */
@RequestMapping("/posts")
@Controller
@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
public class PostController {

	// FIELDS
	private PostService postServ;
	private UserService userServ;
	// CONSTRUCTORS
	/**
	 * No args constructor used by Spring to set up for autowiring.
	 */
	public PostController() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor accepts a {@link UserService} and {@link PostService} class,
	 * expected to be autowired Spring Beans.
	 * 
	 * @param postServ
	 * @param userServ
	 */
	@Autowired
	public PostController(PostService postServ, UserService userServ) {
		super();
		this.postServ = postServ;
		this.userServ = userServ;
	}

	// METHODS

	/**
	 * Selects all Post records from the database.
	 * 
	 * @return
	 */
	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping(value = "/getAllPosts")
	public @ResponseBody List<Post> getAllPosts() {
		return postServ.getAllPosts();
	}

	/**
	 * Accepts an int 'id' Request parameter from the URI to find a post with a
	 * matching ID.
	 * 
	 * @param num
	 * @return
	 */
	@GetMapping(value = "/getPostById")
	public @ResponseBody Post getPostById(@RequestParam("id") int num) {
		return postServ.getPostbyId(num);
	}

	/**
	 * Accepts an int 'author' from the URI to find Posts authored by a User with
	 * that id.
	 * 
	 * @param authorId
	 * @return
	 */
	@GetMapping(value = "/getPostsByAuthor")
	public @ResponseBody List<Post> getPostsByAuthor(@RequestParam("author") int authorId) {
		return postServ.getPostsByAuthorId(authorId);
	}

	/**
	 * Returns a {@link PostPage} object, which contains a {@link Post} and all of
	 * its Comments (currently unused)
	 * 
	 * @param postId
	 * @return
	 */
	@GetMapping(value = "/getPostPage")
	public @ResponseBody PostPage getPostPageInfo(@RequestParam("post") int postId) {
		Post target = postServ.getPostbyId(postId);
		return new PostPage(target, target.getComments());
	}

	/**
	 * Simulates the "like" button being pressed. If the current user has not yet
	 * liked the given {@link Post} then it will add the corresponding {@link User}
	 * object to the Post's collection of likers.
	 * 
	 * @param session
	 * @param response
	 * @param postId
	 * @return
	 */
	@PostMapping(value = "/like/{postId}")
	public @ResponseBody String likePost(HttpSession session, HttpServletResponse response,
			@PathVariable(name = "postId") int postId) {
		SessionUser currentUser = (SessionUser) session.getAttribute("currentUser");

		if (currentUser == null) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You need to be logged in to like a post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "Not logged in!";
		}

		Post targetPost = postServ.getPostbyId(postId);

		if (targetPost == null) {
			try {
				response.sendError(HttpStatus.NOT_FOUND.value(), "You need to be logged in to like a post!");
			} catch (IOException e) {
				e.printStackTrace();
				response.setStatus(HttpStatus.NOT_FOUND.value());
				return "Post not found";
			}
		}

		boolean addedLike = postServ.likeButton(userServ.getUserByUsername(currentUser.getUsername()), targetPost);

		if (addedLike) {
			return "You liked post #" + postId;
		} else {
			return "You un-liked post #" + postId;
		}

	}

	/**
	 * Creates a {@link Post} record in the database with the incoming Post object's
	 * myAuthor field.
	 * 
	 * @param session
	 * @param response
	 * @param incomingPost
	 * @return
	 */
	@PostMapping(value = "/createPostWithUser")
	public @ResponseBody String createNewPost(HttpSession session, HttpServletResponse response,
			@RequestBody Post incomingPost) {

		SessionUser currentUser = (SessionUser) session.getAttribute("currentUser");

		if (currentUser == null) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You need to be logged in to like a post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "Not logged in!";
		}

		if (currentUser.getUsername() != incomingPost.getMyAuthor().getUsername()) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You aren't logged in as the author of this post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "You aren't logged in as the author of this post!";
		}

		postServ.addPost(incomingPost);
		return "Success";
	}

	/**
	 * Creates a {@link Post} object from a given {@link JsonPostInput}.
	 * JsonPostInput consists of a String 'content' and another String 'image'. The
	 * Author of the post is gotten from the currentUser in the session.
	 * 
	 * @param session
	 * @param response
	 * @param incomingPostContent
	 * @return
	 */
	@PostMapping(value = "/createPost")
	public @ResponseBody String createNewPost(HttpSession session, HttpServletResponse response,
			@RequestBody JsonPostInput incomingPost) {

		SessionUser currentUser = (SessionUser) session.getAttribute("currentUser");

		if (currentUser == null) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You need to be logged in to like a post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "Not logged in!";
		}

		Post newPost = new Post(userServ.getUserByUsername(currentUser.getUsername()), incomingPost.getContent(),
				incomingPost.getImage());

		postServ.addPost(newPost);
		return "Success";
	}

	/**
	 * Removes a {@link Post} object from the database if the current user is that
	 * Post's author. Accepts a Post object from the request body.
	 * 
	 * @param session
	 * @param response
	 * @param incomingPost
	 * @return
	 */
	@DeleteMapping(value = "/deletePost")
	public @ResponseBody String deletePost(HttpSession session, HttpServletResponse response,
			@RequestBody Post incomingPost) {
		SessionUser currentUser = (SessionUser) session.getAttribute("currentUser");

		if (currentUser == null) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You need to be logged in to like a post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "Not logged in!";
		}

		if (currentUser.getUsername() != incomingPost.getMyAuthor().getUsername()) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You aren't logged in as the author of this post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "You aren't logged in as the author of this post!";
		}

		postServ.removePost(incomingPost);
		return "Success";
	}

	/**
	 * Removes a {@link Post} object from the database if the current user is that
	 * Post's author. Accepts a {@link JsonSingleInput} with type int containing the
	 * Post's id for identification.
	 * 
	 * @param session
	 * @param response
	 * @param postId
	 * @return
	 */
	@DeleteMapping(value = "/deletePostById")
	public @ResponseBody String deleteUser(HttpSession session, HttpServletResponse response,
			@RequestBody JsonSingleInput<Integer> postId) {
		SessionUser currentUser = (SessionUser) session.getAttribute("currentUser");

		if (currentUser == null) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You need to be logged in to like a post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "Not logged in!";
		}

		Post incomingPost = postServ.getPostbyId(postId.getInput());

		if (currentUser.getUsername() != incomingPost.getMyAuthor().getUsername()) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You aren't logged in as the author of this post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "You aren't logged in as the author of this post!";
		}

		postServ.removePost(incomingPost);
		return "Success";
	}

}
