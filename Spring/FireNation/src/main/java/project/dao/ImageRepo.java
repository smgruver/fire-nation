package project.dao;

import java.util.List;

import project.model.Image;

public interface ImageRepo {

	public void insert(Image myImage);
	
	public Image selectById(int id);
	public List<Image> selectAll();
	public List<Image> selectByPostId(int postId);
	
	public void update(Image image);
	
	public void delete(Image image);
}
