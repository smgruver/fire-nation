package project.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import project.model.Comment;
import project.model.Post;

@Transactional
@Repository("commentRepo")
public class CommentRepoImpl implements CommentRepo {

	private SessionFactory sesFact;
	
	public CommentRepoImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public CommentRepoImpl(SessionFactory sesFact) {
		this.sesFact = sesFact;
	}
	
	@Override
	public void insert(Comment myComment) {
		/*
		 * Session ses = HibernateUtil.getSession(); // database connection Transaction
		 * tx = ses.beginTransaction(); // database transaction
		 * 
		 * ses.save(myComment); // database insert
		 * 
		 * tx.commit(); // commit transaction
		 */	
	
		sesFact.getCurrentSession().save(myComment);
	}

	@Override
	public Comment selectById(int id) {
		/*
		 * Session ses = HibernateUtil.getSession();
		 * 
		 * Comment myComment = ses.get(Comment.class, id);
		 * 
		 * return myComment;
		 */
		
		return sesFact.getCurrentSession().get(Comment.class, id);
	}

	@Override
	public List<Comment> selectByAuthorId(int myAuthorId) {
		/*
		 * Session ses = HibernateUtil.getSession(); List<Comment> authorIdList =
		 * ses.createQuery("from Comment", Comment.class).list(); return authorIdList;
		 */
		
		List<Comment> commentList = sesFact.getCurrentSession().createQuery("from Comment where myAuthor = " + myAuthorId, Comment.class).list();
		//Hibernate.initialize(target.getPosts());

		return commentList;
	}

	@Override
	public List<Comment> selectByPostId(int myPostId) {
		/*
		 * Session ses = HibernateUtil.getSession(); List<Comment> postIdList =
		 * ses.createQuery("from Comment", Comment.class).list(); return postIdList;
		 */
		
		List<Comment> commentList = sesFact.getCurrentSession().createQuery("from Comment where myPost = " + myPostId, Comment.class).list();
		//Hibernate.initialize(target.getPosts());

		return commentList;
	}

	@Override
	public List<Comment> selectAll() {
		/*
		 * Session ses = HibernateUtil.getSession(); List<Comment> commentList =
		 * ses.createQuery("from Comment", Comment.class).list(); return commentList;
		 */
		
		return sesFact.getCurrentSession().createQuery("from Comment", Comment.class).list();
	}

	@Override
	public void update(Comment myComment) {
		/*
		 * Session ses = HibernateUtil.getSession(); Transaction tx =
		 * ses.beginTransaction();
		 * 
		 * ses.update(myComment);
		 * 
		 * tx.commit();
		 */
		
		sesFact.getCurrentSession().saveOrUpdate(myComment);
	}

	@Override
	public void delete(Comment myComment) {
		/*
		 * Session ses = HibernateUtil.getSession(); Transaction tx =
		 * ses.beginTransaction();
		 * 
		 * ses.delete(myComment);
		 * 
		 * tx.commit();
		 */
		
		sesFact.getCurrentSession().delete(myComment);
	}

}
