package project.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import project.exception.NonUniqueValueException;
import project.exception.RecordNotFoundException;
import project.model.User;

@Transactional
@Repository("userRepoImpl")
public class UserRepoImpl implements UserRepo {

	private SessionFactory sesFact;

	public UserRepoImpl() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public UserRepoImpl(SessionFactory sesFact) {
		this.sesFact = sesFact;
	}

	@Override
	public void insert(User myUser) {

		try {
			sesFact.getCurrentSession().save(myUser);
		} catch (DataIntegrityViolationException e) {
			throw new NonUniqueValueException("Either the username or email you tried to add was taken.");
		}
	}

	@Override
	public User selectById(int id) {

		User target = sesFact.getCurrentSession().get(User.class, id);

		if (target == null) {
			return null;
		}
		return target;
	}

	@Override
	public User selectByUsername(String myUsername) {

		List<User> userList = sesFact.getCurrentSession()
				.createQuery("from User where username = '" + myUsername + "'", User.class).list();

		if (userList.size() == 0) {
			return null;
		} else {
			return userList.get(0);
		}
	}

	@Override
	public User selectByEmail(String myEmail) {
		
		List<User> userList = sesFact.getCurrentSession()
				.createQuery("from User where email = '" + myEmail + "'", User.class).list();

		if (userList.size() == 0) {
			return null;
		} else {
			return userList.get(0);
		}
	}

	@Override
	public List<User> selectAll() {

		List<User> targets = sesFact.getCurrentSession().createQuery("from User", User.class).list();

		return targets;
	}

	@Override
	public void update(User myUser) {
		
		try {
			sesFact.getCurrentSession().saveOrUpdate(myUser);
		} catch (DataIntegrityViolationException e) {
			throw new NonUniqueValueException("Either the username or email you tried to add was taken.");
		}
	}

	@Override
	public void delete(User myUser) {

		sesFact.getCurrentSession().delete(myUser);
	}

}