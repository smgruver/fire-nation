package project.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import project.model.Image;

@Transactional
@Repository("imageRepoImpl")
public class ImageRepoImpl implements ImageRepo {

	private SessionFactory sesFact;

	public ImageRepoImpl() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ImageRepoImpl(SessionFactory sesFact) {
		System.out.println("In UserRepoImpl constructo with sesFact");
		this.sesFact = sesFact;
	}
	
	@Override
	public void insert(Image myImage) {
		sesFact.getCurrentSession().save(myImage);
	}

	@Override
	public Image selectById(int id) {
		Image target = sesFact.getCurrentSession().get(Image.class, id);

		return target;
	}

	@Override
	public List<Image> selectAll() {
		List<Image> targets = sesFact.getCurrentSession().createQuery("from Image", Image.class).list();
		/*	//Handles initialization of posts array 
		 * for (User tempUser : targets) { Hibernate.initialize(tempUser.getPosts()); }
		 */

		return targets;
	}

	@Override
	public List<Image> selectByPostId(int postId) {
		return sesFact.getCurrentSession().createQuery("from Image where myPost = " + postId, Image.class).list();
	}

	@Override
	public void update(Image image) {
		sesFact.getCurrentSession().saveOrUpdate(image);
	}

	@Override
	public void delete(Image image) {
		sesFact.getCurrentSession().delete(image);
	}

}
