package project.dao;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import project.model.Post;
import project.model.User;

@Transactional
@Repository("postRepo")
public class PostRepoImpl implements PostRepo {

	private SessionFactory sesFact;

	public PostRepoImpl() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public PostRepoImpl(SessionFactory sesFact) {
		this.sesFact = sesFact;
	}

	@Override
	public void insert(Post myPost) {
		
		sesFact.getCurrentSession().save(myPost);
	}

	@Override
	public Post selectById(int id) {

		Post target = sesFact.getCurrentSession().get(Post.class, id);
		Hibernate.initialize(target.getComments());
		return target;
	}

	@Override
	public List<Post> selectByAuthorId(int myAuthorId) {

		List<Post> postList = sesFact.getCurrentSession().createQuery("from Post where myAuthor = " + myAuthorId, Post.class).list();

		return postList;
	}

	@Override
	public List<Post> selectAll() {

		return sesFact.getCurrentSession().createQuery("from Post", Post.class).list();
	}

	@Override
	public void update(Post myPost) {
		
		sesFact.getCurrentSession().saveOrUpdate(myPost);
	}

	@Override
	public void merge(Post myPost) {
		sesFact.getCurrentSession().merge(myPost);
	}

	@Override
	public void delete(Post myPost) {


		sesFact.getCurrentSession().delete(myPost);
	}

}
