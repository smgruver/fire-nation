package project.model;

/**
 * Used to accept only the necessary information for creating a Post from an incoming HttpRequest's body.
 * Accepts a String 'content' and a String 'image' for the image URL
 * @author sgruv
 *
 */
public class JsonPostInput {

	private int id;
	private String content;
	private String image;
	
	/**
	 * No args constructor used by Spring
	 */
	public JsonPostInput() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Accepts the content of the post and the URL of the image. Used for creating a new Post.
	 * @param content
	 * @param image
	 */
	public JsonPostInput(String content, String image) {
		super();
		this.content = content;
		this.image = image;
	}
	
	/**
	 * Accepts the content and id of the post. Used for editing an existing Post.
	 * @param content
	 * @param image
	 */
	public JsonPostInput(int id, String content) {
		super();
		this.id = id;
		this.content = content;
		this.image = null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "JsonPostInput [id=" + id + ", content=" + content + ", image=" + image + "]";
	}

}
