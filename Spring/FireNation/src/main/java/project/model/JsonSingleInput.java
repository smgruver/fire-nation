package project.model;

public class JsonSingleInput<T> {

	private T input;
	
	public JsonSingleInput() {
		// TODO Auto-generated constructor stub
	}
	
	public JsonSingleInput(T input) {
		this.input = input;
	}

	public T getInput() {
		return input;
	}

	public void setInput(T input) {
		this.input = input;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((input == null) ? 0 : input.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JsonSingleInput other = (JsonSingleInput) obj;
		if (input == null) {
			if (other.input != null)
				return false;
		} else if (!input.equals(other.input))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JsonSingleInput [input=" + input + "]";
	}

}
