package project.model;

/**
 * Used for editing the information of a {@link User} object's record, besides
 * the Password. To edit a User's password, {@link EditUserPasswordRequestBody}
 * is required. Any fields left null will retain the current value of that field
 * in the User record.
 * 
 * @author sgruv
 *
 */
public class EditUserBody {

	private String username;
	private String firstName;
	private String lastName;
	private String email;

	/**
	 * Default constructor used by Spring
	 */
	public EditUserBody() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Accepts all mutable fields of a {@link User} object except the password. 
	 * @param username
	 * @param firstName
	 * @param lastName
	 * @param email
	 */
	public EditUserBody(String username, String firstName, String lastName, String email) {
		super();
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "EditUserBody [username=" + username + ", firstName=" + firstName + ", lastName=" + lastName + ", email="
				+ email + "]";
	}
}
