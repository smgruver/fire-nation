package project.log4j;

import org.apache.log4j.Logger;

public class preTest {
	
	final static Logger log = Logger.getLogger(preTest.class);

	public static void main(String[] args) {
		log.warn("Pre-Testing is effect and is currently logged");
		log.error("This is an error you can get when testing.",new IllegalArgumentException());
		log.fatal("Testing is about to begin and will abruptly stop once completed");
	}
}
