import static org.junit.Assert.*;

import java.net.MalformedURLException;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import project.log4j.preTest;
import project.model.Comment;
import project.model.EditUserBody;
import project.model.EditUserPasswordRequestBody;
import project.model.Image;
import project.model.Post;
import project.model.PostPage;
import project.model.SessionUser;
import project.model.User;

public class projectTest {
	@Test
	public void NewUserTestModel() {
		User use = new User("user1","12345","John","Doe","12345@gmail.com");
		assertEquals("user1",use.getUsername());
		assertEquals("12345",use.getPassword());
		assertEquals("John",use.getFirstName());
		assertEquals("Doe",use.getLastName());
		assertEquals("12345@gmail.com",use.getEmail());
	}
	
	@Test 
	public void UserPageTestModel() {
		SessionUser sesUse = new SessionUser("user1","12345");
		assertEquals("user1",sesUse.getUsername());
		assertEquals("12345",sesUse.getPassword());
	}
	
	@Test 
	public void PostTest1Model() {
		User use = new User("user1","12345","John","Doe","12345@gmail.com");
		Post p = new Post(use,"Content");
		assertEquals(use,use);
		assertEquals("Content",p.getContent());
	}
	
	@Test 
	public void PostTest2Model() {
		User use = new User("user1","12345","John","Doe","12345@gmail.com");
		Post p = new Post(use,"Content","Image1");
		assertEquals(use,use);
		assertEquals("Content",p.getContent());
		assertEquals("Image1",p.getImage());
	}
	
	@Test 
	public void NewImageTestModel() {
		Image img = new Image(1,"source1");
		assertEquals(1,img.getMyPost());
		assertEquals("source1",img.getSource());
	}
	
	@Test 
	public void ExistingImageTestModel() {
		Image img = new Image(1,1,"source1");
		assertEquals(1,img.getId());
		assertEquals(1,img.getMyPost());
		assertEquals("source1",img.getSource());
	}
	
	@Test 
	public void EditUserPasswordRequestBodyTestModel() {
		EditUserPasswordRequestBody euprb = new EditUserPasswordRequestBody("password1","password2");
		assertEquals("password1",euprb.getOldPassword());
		assertEquals("password2",euprb.getNewPassword());
	}
	
	@Test
	public void CommentTestModel() {
		Comment com = new Comment(1,4,"New Comment");
		assertEquals(1,com.getMyAuthor());
		assertEquals(4,com.getMyPost());
		assertEquals("New Comment",com.getContent());
	}
	
	@Test 
	public void SetIdTestCommentModel() {
		Comment com = new Comment();
		com.setId(1);
		assertTrue(com.getId()==1);
	}
	
	@Test 
	public void SetContentTestCommentModel() {
		Comment com = new Comment();
		com.setContent("New Comment");
		assertTrue(com.getContent()=="New Comment");
	}
	
	@Test 
	public void SetMyAuthorTestCommentModel() {
		Comment com = new Comment();
		com.setMyAuthor(1);
		assertTrue(com.getMyAuthor()==1);
	}
	
	@Test 
	public void SetMyPostTestCommentModel() {
		Comment com = new Comment();
		com.setMyPost(1);
		assertTrue(com.getMyPost()==1);
	}
	
	@Test 
	public void SetEmailTestEditUserModel() {
		EditUserBody eub = new EditUserBody();
		eub.setEmail("12345@gmail.com");
		assertTrue(eub.getEmail()=="12345@gmail.com");
	}
	
	@Test 
	public void SetFirstnameEditUserModel() {
		EditUserBody eub = new EditUserBody();
		eub.setFirstName("John");
		assertTrue(eub.getFirstName()=="John");	
	}
	
	@Test 
	public void SetLastnameEditUserModel() {
		EditUserBody eub = new EditUserBody();
		eub.setLastName("Doe");
		assertTrue(eub.getLastName()=="Doe");	
	}
	
	@Test 
	public void SetOldPasswordEditUserPasswordRequestBodyModel() {
		EditUserPasswordRequestBody euprb = new EditUserPasswordRequestBody();
		euprb.setOldPassword("password1");
		assertTrue(euprb.getOldPassword()=="password1");	
	}
	
	@Test 
	public void  SetNewPasswordEditUserPasswordRequestBodyModel() {
		EditUserPasswordRequestBody euprb = new EditUserPasswordRequestBody();
		euprb.setNewPassword("password2");
		assertTrue(euprb.getNewPassword()=="password2");	
	}
	
	@Test 
	public void SetUsernameEditUserModel() {
		EditUserBody eub = new EditUserBody();
		eub.setUsername("user1");
		assertTrue(eub.getUsername()=="user1");	
	}
	
	
	@Test 
	public void SetIDImageModel() {
		Image img = new Image();
		img.setId(1);
		assertTrue(img.getId()==1);	
	}
	
	@Test 
	public void SeMyPostImageModel() {
		Image img = new Image();
		img.setMyPost(1);
		assertTrue(img.getMyPost()==1);	
	}
	
	@Test 
	public void SetSourceImageModel() {
		Image img = new Image();
		img.setSource("Source1");
		assertTrue(img.getSource()=="Source1");	
	}
	
	@Test 
	public void SetIDPostModel() {
		Post post = new Post();
		post.setId(1);
		assertTrue(post.getId()==1);	
	}
	
	@Test 
	public void SetImagePostModel() {
		Post post = new Post();
		post.setImage("image1");
		assertTrue(post.getImage()=="image1");	
	}
	
	@Test 
	public void SetContentPostModel() {
		Post post = new Post();
		post.setContent("content1");
		assertTrue(post.getContent()=="content1");	
	}
	
	@Test 
	public void SetMyAuthorPostModel() {
		User use = new User();
		Post post = new Post();
		post.setMyAuthor(use);
		assertTrue(post.getMyAuthor()==use);	
	}
	
	@Test 
	public void SetPostPostPageModel() {
		Post post = new Post();
		PostPage pp = new PostPage();
		pp.setMyPost(post);
		assertTrue(pp.getMyPost()==post);	
	}
}
