package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "user_info")
public class User {

	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int user_id;

	@Column(name = "username", nullable = false, unique = true)
	private String user_username;

	@Column(name = "user_password", nullable = false)
	private String user_password;

	@Column(name = "first_name", nullable = false)
	private String user_firstname;

	@Column(name = "last_name", nullable = false)
	private String user_lastname;

	@Column(name = "email", nullable = false, unique = true)
	private String user_email;

	@Column(name = "profile_pic")
	private String user_image;

	// No Args
	public User() {
		// TODO Auto-generated constructor stub
	}

	// No Primary Key
	public User(String user_username, String user_password, String user_firstname, String user_lastname,
			String user_email) {
		super();
		this.user_id = user_id;
		this.user_username = user_username;
		this.user_password = user_password;
		this.user_firstname = user_firstname;
		this.user_lastname = user_lastname;
		this.user_email = user_email;
		this.user_image = null;
	}

	// No Primary Key
	public User(String user_username, String user_password, String user_firstname, String user_lastname,
			String user_email, String user_image) {
		super();
		this.user_username = user_username;
		this.user_password = user_password;
		this.user_firstname = user_firstname;
		this.user_lastname = user_lastname;
		this.user_email = user_email;
		this.user_image = user_image;
	}

	// Existing user; All Args
	public User(int user_id, String user_username, String user_password, String user_firstname, String user_lastname,
			String user_email, String user_image) {
		super();
		this.user_id = user_id;
		this.user_username = user_username;
		this.user_password = user_password;
		this.user_firstname = user_firstname;
		this.user_lastname = user_lastname;
		this.user_email = user_email;
		this.user_image = user_image;
	}

	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", user_username=" + user_username + ", user_password=" + user_password
				+ ", user_firstname=" + user_firstname + ", user_lastname=" + user_lastname + ", user_email="
				+ user_email + ", user_image=" + user_image + "]";
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUser_username() {
		return user_username;
	}

	public void setUser_username(String user_username) {
		this.user_username = user_username;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public String getUser_firstname() {
		return user_firstname;
	}

	public void setUser_firstname(String user_firstname) {
		this.user_firstname = user_firstname;
	}

	public String getUser_lastname() {
		return user_lastname;
	}

	public void setUser_lastname(String user_lastname) {
		this.user_lastname = user_lastname;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getUser_image() {
		return user_image;
	}

	public void setUser_image(String user_image) {
		this.user_image = user_image;
	}

}
