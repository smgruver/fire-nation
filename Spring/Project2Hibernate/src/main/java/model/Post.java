package model;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name="post")
public class Post {

	@Id
	@Column(name="post_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	//@Column(name="user_id", nullable=false)
	private User myAuthor;
	
	@Column(name="post_content", nullable=false)
	private String content;
	
	@Column(name="post_time", nullable = false)
	private Timestamp creationDate;
	
	@Column(name="post_image")
	private String image;
	
	@OneToMany(mappedBy="myPost", fetch = FetchType.LAZY)
	private List<Comment> comments = new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<User> likes;
	
	public Post() {
		// TODO Auto-generated constructor stub
	}
	
	public Post(User myAuthor, String content) {
		super();
		this.myAuthor = myAuthor;
		this.content = content;
		this.creationDate = Timestamp.valueOf(LocalDateTime.now());
		this.image = null;
		this.comments = new ArrayList<Comment>();
		this.likes = new ArrayList<User>();
	}
	
	public Post(User myAuthor, String content, String image) {
		super();
		this.myAuthor = myAuthor;
		this.content = content;
		this.creationDate = Timestamp.valueOf(LocalDateTime.now());
		this.image = image;
		this.comments = new ArrayList<Comment>();
		this.likes = new ArrayList<User>();
	}

	public Post(User myAuthor, String content, Timestamp creationDate, String image, List<Comment> comments,
			List<User> likers) {
		super();
		this.myAuthor = myAuthor;
		this.content = content;
		this.creationDate = creationDate;
		this.image = image;
		this.comments = comments;
		this.likes = likers;
	}
	
	public Post(int id, User myAuthor, String content, Timestamp creationDate, String image, List<Comment> comments,
			List<User> likers) {
		super();
		this.id = id;
		this.myAuthor = myAuthor;
		this.content = content;
		this.creationDate = creationDate;
		this.image = image;
		this.comments = comments;
		this.likes = likers;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getMyAuthor() {
		return myAuthor;
	}

	public void setMyAuthor(User myAuthor) {
		this.myAuthor = myAuthor;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public List<User> getLikes() {
		return likes;
	}

	public void setLikes(List<User> likes) {
		this.likes = likes;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", myAuthor=" + myAuthor.getUser_username() + ", content=" + content + ", creationDate=" + creationDate
				+ ", image=" + image + ", comments=" + comments.size() + ", likes=" + likes.size() + "]";
	}
}
