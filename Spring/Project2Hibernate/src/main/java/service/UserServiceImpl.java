package service;

import java.util.List;

import dao.UserDao;
import dao.UserDaoImpl;
import model.User;

public class UserServiceImpl implements UserService {

	UserDao userDao = new UserDaoImpl();
	
	@Override
	public void addUser(User myUser) {
		userDao.insert(myUser);
	}

	@Override
	public List<User> getAllUsers() {
		return userDao.selectAll();
	}

	@Override
	public void editUser(User myUser) {
		userDao.update(myUser);
	}

	@Override
	public void removeUser(User myUser) {
		userDao.delete(myUser);
	}

}
