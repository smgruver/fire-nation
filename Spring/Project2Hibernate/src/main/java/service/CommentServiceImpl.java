package service;

import java.util.List;

import dao.CommentDao;
import dao.CommentDaoImpl;
import model.Comment;

public class CommentServiceImpl implements CommentService {

	CommentDao commentDao = new CommentDaoImpl();
	
	@Override
	public void addComment(Comment myComment) {
		commentDao.insert(myComment);	
	}

	@Override
	public List<Comment> getAllComments() {
		return commentDao.selectAll();
	}

	@Override
	public void editComment(Comment myComment) {
		commentDao.update(myComment);
		
	}

	@Override
	public void removeUser(Comment myComment) {
		commentDao.delete(myComment);
	}

	@Override
	public Comment getById(int id) {
		return commentDao.selectById(id);
		
	}

}
