package service;

import java.util.List;

import model.Image;

public interface ImageService {

	public void addImage(Image myImage);
	
	public List<Image> getAllImages();
	
	public void editImage(Image myImage);
	
	public void removeImage(Image myImage);	
}
