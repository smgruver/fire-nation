package service;

import java.util.List;

import model.Post;

public interface PostService {

	public void addPost(Post myPost);
	
	public List<Post> getAllPosts();
	
	public void editPost(Post myPost);
	
	public void removePost(Post myPost);
}
