package service;

import java.util.List;

import dao.ImageDao;
import dao.ImageDaoImpl;
import model.Image;

public class ImageServiceImpl implements ImageService {

	ImageDao imageDao = new ImageDaoImpl();
	
	@Override
	public void addImage(Image myImage) {
		imageDao.insert(myImage);
	}

	@Override
	public List<Image> getAllImages() {
		return imageDao.selectAll();
	}

	@Override
	public void editImage(Image myImage) {
		imageDao.update(myImage);
	}

	@Override
	public void removeImage(Image myImage) {
		imageDao.delete(myImage);
	}

}
