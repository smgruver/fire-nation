package service;

import java.util.List;

import model.User;

public interface UserService {

	public void addUser(User myUser);
	
	public List<User> getAllUsers();
	
	public void editUser(User myUser);
	
	public void removeUser(User myUser);	
}
