import java.time.LocalDateTime;

import model.Comment;
import model.Image;
import model.Post;
import model.User;
import service.CommentService;
import service.CommentServiceImpl;
import service.ImageService;
import service.ImageServiceImpl;
import service.PostService;
import service.PostServiceImpl;
import service.UserService;
import service.UserServiceImpl;
import util.HibernateUtil;

public class MainDriver {

	public static UserService userv = new UserServiceImpl();
	public static PostService pserv = new PostServiceImpl();
	public static CommentService cserv = new CommentServiceImpl();
	public static ImageService iserv = new ImageServiceImpl();
	
	public static void main(String[] args) {
		insertInitialValues();
		
		System.out.println("Selecting all users");
		System.out.println(userv.getAllUsers());
		
		System.out.println("Selecting all posts");
		System.out.println(pserv.getAllPosts());
		
		System.out.println("Selecting all comments");
		System.out.println(cserv.getAllComments());
		
		System.out.println("Selecting all images");
		System.out.println(iserv.getAllImages());
		
		//update();

		HibernateUtil.closeSession();
	}
	
	private static void insertInitialValues() {
		User user1 = new User("username1", "password1", "firstname1", "lastname1",
				"email1@ema.il");
		User user2 = new User("username2", "password2", "firstname2", "lastname2","email2@ema.il");
		
		Post post1 = new Post(user1, "the first post", null);
		Post post2 = new Post(user2, "the second post", null);
		
		Comment comment1 = new Comment(user1, post1, "the first comment", LocalDateTime.now());
		Comment comment2 = new Comment(user2, post2, "the second comment", LocalDateTime.now());
		
//		Image image1 = new Image(post1, "https://vignette.wikia.nocookie.net/new-game/images/e/e9/Th-c4.png/revision/latest?cb=20180121131349");
//		Image image2 = new Image(post2, "https://vignette.wikia.nocookie.net/fategrandorder/images/5/54/Portrait_Servant_196_1.png/revision/latest?cb=20171215134259");
		
//		post1.getImages().add(image1);
//		post2.getImages().add(image2);
		
		post1.getComments().add(comment1);
		post2.getComments().add(comment2);
		
		post1.getLikes().add(user2);
		post2.getLikes().add(user1);
		
//		iserv.addImage(image1);
//		iserv.addImage(image2);
		
		cserv.addComment(comment1);
		cserv.addComment(comment2);
		
		pserv.addPost(post1);
		pserv.addPost(post2);
	
		userv.addUser(user1);
		userv.addUser(user2);
		
		
	}
	
	private static void update() {
		Comment newComment = cserv.getById(1);
		newComment.setContent("Testing #2");
		cserv.editComment(newComment);
		
		System.out.println(newComment);
		System.out.println(cserv.getById(1));	
	}
	
	
	
}
