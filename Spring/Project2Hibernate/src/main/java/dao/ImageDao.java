package dao;

import java.util.List;

import model.Image;

public interface ImageDao {

	public void insert(Image myImage);
	
	public Image selectById(int id);
	public List<Image> selectAll();
	public List<Image> selectByPostId(int postId);
	
	public void update(Image image);
	
	public void delete(Image image);
}
