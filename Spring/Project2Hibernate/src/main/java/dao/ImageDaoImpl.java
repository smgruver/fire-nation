package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.Image;
import model.User;
import util.HibernateUtil;

public class ImageDaoImpl implements ImageDao {

	@Override
	public void insert(Image myImage) {
		Session ses = HibernateUtil.getSession(); // database connection
		Transaction tx = ses.beginTransaction(); // database transaction

		ses.save(myImage); // database insert

		tx.commit(); // commit transaction
	}

	@Override
	public Image selectById(int id) {
		Session ses = HibernateUtil.getSession();

		Image myImage = ses.get(Image.class, id);

		return myImage;
	}

	@Override
	public List<Image> selectAll() {
		Session ses = HibernateUtil.getSession();
		List<Image> imageList = ses.createQuery("from Image", Image.class).list();
		return imageList;
	}

	@Override
	public List<Image> selectByPostId(int postId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Image image) {
		Session ses = HibernateUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.update(image);
		
		tx.commit();
	}

	@Override
	public void delete(Image image) {
		Session ses = HibernateUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.delete(image);
		
		tx.commit();
	}

}
