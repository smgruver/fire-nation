package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.User;
import util.HibernateUtil;

public class UserDaoImpl implements UserDao {

	@Override
	public void insert(User myUser) {
		Session ses = HibernateUtil.getSession(); // database connection
		Transaction tx = ses.beginTransaction(); // database transaction

		ses.save(myUser); // database insert

		tx.commit(); // commit transaction
	}

	@Override
	public User selectById(int id) {
		Session ses = HibernateUtil.getSession();

		User myUser = ses.get(User.class, id);

		return myUser;
	}

	@Override
	public User selectByUsername(String myUsername) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User selectByEmail(String myEmail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> selectAll() {
		Session ses = HibernateUtil.getSession();
		List<User> userList = ses.createQuery("from User", User.class).list();
		return userList;

	}

	@Override
	public void update(User myUser) {
		Session ses = HibernateUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.update(myUser);
		
		tx.commit();
	}

	@Override
	public void delete(User myUser) {
		Session ses = HibernateUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.delete(myUser);
		
		tx.commit();
	}

}
