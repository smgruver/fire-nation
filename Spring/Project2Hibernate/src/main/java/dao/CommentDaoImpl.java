package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.Comment;
import util.HibernateUtil;

public class CommentDaoImpl implements CommentDao {

	@Override
	public void insert(Comment myComment) {
		Session ses = HibernateUtil.getSession(); // database connection
		Transaction tx = ses.beginTransaction(); // database transaction

		ses.save(myComment); // database insert

		tx.commit(); // commit transaction
	}

	@Override
	public Comment selectById(int id) {
		Session ses = HibernateUtil.getSession();

		Comment myComment = ses.get(Comment.class, id);

		return myComment;
	}

	@Override
	public List<Comment> selectByAuthorId(int myAuthorId) {
		Session ses = HibernateUtil.getSession();
		List<Comment> authorIdList = ses.createQuery("from Comment", Comment.class).list();
		return authorIdList;
	}

	@Override
	public List<Comment> selectByPostId(int myPostId) {
		Session ses = HibernateUtil.getSession();
		List<Comment> postIdList = ses.createQuery("from Comment", Comment.class).list();
		return postIdList;
	}

	@Override
	public List<Comment> selectAll() {
		Session ses = HibernateUtil.getSession();
		List<Comment> commentList = ses.createQuery("from Comment", Comment.class).list();
		return commentList;
	}

	@Override
	public void update(Comment myComment) {
		Session ses = HibernateUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.update(myComment);
		
		tx.commit();
	}

	@Override
	public void delete(Comment myComment) {
		Session ses = HibernateUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.delete(myComment);
		
		tx.commit();
	}

}
