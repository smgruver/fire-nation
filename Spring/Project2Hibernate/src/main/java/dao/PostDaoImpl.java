package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import model.Comment;
import model.Post;
import util.HibernateUtil;

public class PostDaoImpl implements PostDao {

	@Override
	public void insert(Post myPost) {
		Session ses = HibernateUtil.getSession(); // database connection
		Transaction tx = ses.beginTransaction(); // database transaction

		ses.save(myPost); // database insert

		tx.commit(); // commit transaction
		
	}

	@Override
	public Post selectById(int id) {
		Session ses = HibernateUtil.getSession();

		Post myPost = ses.get(Post.class, id);

		return myPost;
	}

	@Override
	public List<Post> selectByAuthorId(int myAuthorId) {
		Session ses = HibernateUtil.getSession();
		List<Post> authorIdList = ses.createQuery("from Post", Post.class).list();
		return authorIdList;
	}

	@Override
	public List<Post> selectAll() {
		Session ses = HibernateUtil.getSession();
		List<Post> postList = ses.createQuery("from Post", Post.class).list();
		return postList;
	}

	@Override
	public void update(Post myPost) {
		Session ses = HibernateUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.update(myPost);
		
		tx.commit();
	}

	@Override
	public void delete(Post myPost) {
		Session ses = HibernateUtil.getSession();
		Transaction tx = ses.beginTransaction();
		
		ses.delete(myPost);
		
		tx.commit();
	}

}
