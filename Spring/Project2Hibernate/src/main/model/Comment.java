package model;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="post_comment")
public class Comment {

	@Id
	@Column(name="comment_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User myAuthor;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="post_id")
	private Post myPost;

	@Column(name="comment_content", nullable = false)
	private String content;
	
	
	@Column(name="comment_date", nullable = false)
	private LocalDateTime creationDate;
	
	public Comment() {
		// TODO Auto-generated constructor stub
	}

	public Comment(User myAuthor, Post post, String content, LocalDateTime creationDate) {
		super();
		this.myAuthor = myAuthor;
		this.myPost = post;
		this.content = content;
		this.creationDate = creationDate;
	}
	
	public Comment(int id, User myAuthor, Post post, String content, LocalDateTime creationDate) {
		super();
		this.id = id;
		this.myAuthor = myAuthor;
		this.myPost = post;
		this.content = content;
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", myAuthor=" + myAuthor + ", post=" + myPost + ", content=" + content
				+ ", creationDate=" + creationDate + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public User getAuthorId() {
		return myAuthor;
	}

	public void setAuthorId(User myAuthor) {
		this.myAuthor = myAuthor;
	}

	public Post getPost() {
		return myPost;
	}

	public void setPost(Post post) {
		this.myPost = post;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}
}
