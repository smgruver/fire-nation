package service;

import java.util.List;

import dao.PostDao;
import dao.PostDaoImpl;
import model.Post;

public class PostServiceImpl implements PostService {

	PostDao postDao = new PostDaoImpl();
	
	@Override
	public void addPost(Post myPost) {
		postDao.insert(myPost);	
	}

	@Override
	public List<Post> getAllPosts() {
		return postDao.selectAll();
	}

	@Override
	public void editPost(Post myPost) {
		postDao.update(myPost);
		
	}

	@Override
	public void removePost(Post myPost) {
		postDao.delete(myPost);
		
	}

}
