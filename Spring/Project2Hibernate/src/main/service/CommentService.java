package service;

import java.util.List;

import model.Comment;

public interface CommentService {

	public void addComment(Comment myComment);
	
	public List<Comment> getAllComments();
	
	public void editComment(Comment myComment);
	
	public void removeUser(Comment myComment);
	
	public Comment getById(int id);
}
