package dao;

import java.util.List;

import model.User;

public interface UserDao {

	public void insert(User myUser);
	
	public User selectById(int id);
	public User selectByUsername(String myUsername);
	public User selectByEmail(String myEmail);
	public List<User> selectAll();
	
	public void update(User myUser);
	
	public void delete(User myUser);
}
