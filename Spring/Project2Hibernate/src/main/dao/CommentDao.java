package dao;

import java.util.List;

import model.Comment;

public interface CommentDao {
	
	public void insert(Comment myComment);
	public Comment selectById(int id);
	public List<Comment> selectByAuthorId(int myAuthorId);
	public List<Comment> selectByPostId(int myPostId);
	public List<Comment> selectAll();
	public void update(Comment myPost);
	public void delete(Comment myPost);

}
