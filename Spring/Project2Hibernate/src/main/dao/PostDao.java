package dao;

import java.util.List;

import model.Post;

public interface PostDao {

	public void insert(Post myPost);
	public Post selectById(int id);
	public List<Post> selectByAuthorId(int myAuthorId);
	public List<Post> selectAll();
	public void update(Post myPost);
	public void delete(Post myPost);
}
