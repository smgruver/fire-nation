package project.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.controller.PostController;
import project.dao.PostRepo;
import project.dao.UserRepo;
import project.exception.MissingNecessaryValueException;
import project.model.JsonPostInput;
import project.model.Post;
import project.model.User;

/**
 * Contains methods called by {@link PostController} to perform business logic and interface with {@link PostRepo}.
 * @author sgruv
 *
 */
@Service("postServ")
public class PostServiceImpl implements PostService {

	private PostRepo postRepo;
	private UserRepo userRepo;

	/**
	 * No args constructor used by Spring to set up for autowiring.
	 */
	public PostServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Accepts a {@link UserRepo} and {@link PostRepo}, expected to be Spring Beans.
	 * @param userRepo
	 * @param postRepo
	 */
	@Autowired
	public PostServiceImpl(PostRepo postRepo, UserRepo userRepo) {
		this.postRepo = postRepo;
		this.userRepo = userRepo;
	}
	
	/**
	 * Accepts a {link Post} object, sets the creationDate to 'now', and passes it to {@link PostRepo} to be added to the database.
	 * Throws a {@link MissingNecessaryValueException} if the Post's content field is null.
	 * @param myPost the Post to be added
	 * @return void
	 */
	@Override
	public void addPost(Post myPost) throws MissingNecessaryValueException{
		myPost.setCreationDate(Timestamp.valueOf(LocalDateTime.now()));
		
		if (myPost.getContent() == null) {
			throw new MissingNecessaryValueException("Cannot create a post without content");
		}
		
		postRepo.insert(myPost);	
	}

	/**
	 * Uses {@link PostRepo}'s selectAll method to return all {@link Post} records from the database.
	 * @return List<Post>
	 */
	@Override
	public List<Post> getAllPosts() {
		return postRepo.selectAll();
	}

	/**
	 * Accepts an int 'id' and uses {@link PostRepo}'s selectById method to return a {@link Post} with a matching post_id.
	 * @param id the id of the Post to be found
	 * @return Post
	 */
	@Override
	public Post getPostbyId(int id) {
		return postRepo.selectById(id);
	}

	/**
	 * Accepts an int "myAuthorId" and passes it to {@link PostRepo}'s selectByAuthorId method to return all {@link Post} objects authored by the corresponding {@link User}.
	 * param myAuthorId the id of the User whose posts are to be found
	 * @return List<Post>
	 */
	@Override
	public List<Post> getPostsByAuthorId(int myAuthorId) {
		return postRepo.selectByAuthorId(myAuthorId);
	}
	
	/**
	 * Accepts a {@link Post} object, and replaces the content of the corresponding record with the incoming Post's content.
	 * @param myPost a Post with the same id as the one to be edited, but new content
	 * @return void
	 */
	@Override
	public void editPost(JsonPostInput myPost) {
		Post oldPost = postRepo.selectById(myPost.getId());
		oldPost.setContent(myPost.getContent());

		postRepo.update(oldPost);
		
	}

	/**
	 * Contains the business logic for the like button functionality. Checks whether the user would be liking or unliking based on whether or not they already like the post.
	 * @param liker the current User who is liking or unlike a Post
	 * @param targetPost the Post to be liked or unliked
	 * @return boolean whether a like was added (true) or removed (false).
	 */
	@Override
	public boolean likeButton(User liker, Post targetPost) {
		boolean addedLike;	//Tracks whether a like was added or removed so the controller can determine which message to send.
		
		if (targetPost.getLikes().contains(liker)) {	//.equals( ) for Users has been overloaded so this works now
			System.out.println("You already like this post!");
			targetPost.getLikes().remove(liker);	//.equals( ) for Users has been overloaded so this works now
			addedLike = false;
		} else {
			targetPost.getLikes().add(liker);
			addedLike = true;
		}
		
		postRepo.merge(targetPost);		//merge( ) instead of update( ) because I persisted the post with a .get( )
		return addedLike;
	}

	/**
	 * Accepts a {@link Post} and passes it {@link PostRepo}'s delete method to remove the corresponding record from the database.
	 * @param myPost the post to be removed from the database
	 * @return void
	 */
	@Override
	public void removePost(Post myPost) {
		postRepo.delete(myPost);
		
	}
}
