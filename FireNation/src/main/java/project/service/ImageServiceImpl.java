package project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.dao.ImageRepo;
import project.dao.PostRepo;
import project.model.Image;

@Service("imageServ")
public class ImageServiceImpl implements ImageService {

	private ImageRepo imageRepo;
	
	public ImageServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ImageServiceImpl(ImageRepo imageRepo) {
		this.imageRepo = imageRepo;
	}
	
	@Override
	public void addImage(Image myImage) {
		imageRepo.insert(myImage);
	}

	@Override
	public Image getImageById(int id) {
		return imageRepo.selectById(id);
	}

	@Override
	public List<Image> getAllImages() {
		return imageRepo.selectAll();
	}
	
	@Override
	public List<Image> getImagesByPostId(int id) {
		return imageRepo.selectByPostId(id);
	}	

	@Override
	public void editImage(Image myImage) {
		imageRepo.update(myImage);
	}

	@Override
	public void removeImage(Image myImage) {
		imageRepo.delete(myImage);
	}
}
