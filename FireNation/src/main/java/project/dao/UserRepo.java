package project.dao;

import java.util.List;

import project.exception.MissingNecessaryValueException;
import project.model.User;

public interface UserRepo {

	public void insert(User myUser);
	
	public User selectById(int id);
	public User selectByUsername(String myUsername);
	public User selectByEmail(String myEmail);
	public List<User> selectAll();
	
	public void update(User myUser);
	
	public void delete(User myUser);
}
