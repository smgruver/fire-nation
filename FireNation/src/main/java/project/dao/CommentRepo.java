package project.dao;

import java.util.List;

import project.model.Comment;

public interface CommentRepo {
	
	public void insert(Comment myComment);
	
	public Comment selectById(int id);
	public List<Comment> selectAll();
	public List<Comment> selectByAuthorId(int myAuthorId);
	public List<Comment> selectByPostId(int myPostId);
	
	public void update(Comment myPost);
	
	public void delete(Comment myPost);
}
