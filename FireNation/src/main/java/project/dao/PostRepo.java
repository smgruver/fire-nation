package project.dao;

import java.util.List;

import project.model.Post;

public interface PostRepo {

	public void insert(Post myPost);
	
	public Post selectById(int id);
	public List<Post> selectByAuthorId(int myAuthorId);
//	public List<Post> selectByLikes(int likes);		//unnecessary; if we want the users who liked a post we just need to get the post and use the getLikers() method
	public List<Post> selectAll();
	
	public void update(Post myPost);
	public void merge(Post myPost);
	
	public void delete(Post myPost);
}
