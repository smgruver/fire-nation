package project.model;

/**
 * Made to accept a pair of strings- username and password- from the body of a HttpRequest.
 * Used in logging in and checking login status.
 * @author sgruv
 *
 */
public class SessionUser {

	private String username;
	private String password;
	
	/**
	 * No args constructor used by Spring
	 */
	public SessionUser() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Accepts a username and password to be checked against User records for login.
	 * @param username
	 * @param password
	 */
	public SessionUser(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "SessionUser [username=" + username + ", password=" + password + "]";
	}
}
