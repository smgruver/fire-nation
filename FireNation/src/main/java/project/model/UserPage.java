package project.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Contains all of the information necessary for printing a complete user profile page.
 * Includes the {@link User} object, and two {@link List} fields containing {@link Post} objects;
 * one for the posts the user has authored and another for the posts the user has liked.
 * @author sgruv
 *
 */
public class UserPage {

	private User myUser;
	
	private List<Post> userPosts;	//posts the user has authored
	
	private List<Post> userLikes;	//posts the user has liked
	
	/**
	 * No args constructor used by Spring.
	 */
	public UserPage() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Accepts the user and a list of posts. Used if we don't want to (or don't have a means to) access the user's likes.
	 * @param myUser
	 * @param userPosts
	 */
	public UserPage(User myUser, Collection<Post> userPosts) {
		super();
		this.myUser = myUser;
		this.userPosts = new ArrayList<>(userPosts);
		this.userLikes = null;
	}
	
	/**
	 * Accepts all parameters for a complete user page.
	 * @param myUser
	 * @param userPosts
	 * @param userLikes
	 */
	public UserPage(User myUser, Collection<Post> userPosts, Collection<Post> userLikes) {
		super();
		this.myUser = myUser;
		this.userPosts = new ArrayList<>(userPosts);
		this.userLikes = new ArrayList<>(userLikes);
	}

	public User getMyUser() {
		return myUser;
	}

	public void setMyUser(User myUser) {
		this.myUser = myUser;
	}

	public List<Post> getUserPosts() {
		return userPosts;
	}

	public void setUserPosts(List<Post> userPosts) {
		this.userPosts = userPosts;
	}

	public List<Post> getUserLikes() {
		return userLikes;
	}

	public void setUserLikes(List<Post> userLikes) {
		this.userLikes = userLikes;
	}

	@Override
	public String toString() {
		return "UserPage [myUser=" + myUser + ", userPosts=" + userPosts + ", userLikes=" + userLikes + "]";
	}
}
