package project.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
@Entity
@Table(name="post_comment")
public class Comment {

	@Id
	@Column(name="comment_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
//	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinColumn(name="user_id")
	@Column(name = "user_id", nullable = false)
	private int myAuthor;
	
//	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinColumn(name="post_id")
	@Column(name = "post_id", nullable = false)
	private int myPost;

	@Column(name="comment_content", nullable = false)
	private String content;
	
	@Column(name="comment_date", nullable = false)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
	private Timestamp creationDate;
	
	public Comment() {
		// TODO Auto-generated constructor stub
	}

	//New Comment
	public Comment(int myAuthor, int post, String content) {
		super();
		this.myAuthor = myAuthor;
		this.myPost = post;
		this.content = content;
		this.creationDate = Timestamp.valueOf(LocalDateTime.now());
	}
	
	//Existing Comment
	public Comment(int id, int myAuthor, int post, String content, Timestamp creationDate) {
		super();
		this.id = id;
		this.myAuthor = myAuthor;
		this.myPost = post;
		this.content = content;
		this.creationDate = creationDate;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", myAuthor=" + myAuthor + ", post=" + myPost + ", content=" + content
				+ ", creationDate=" + creationDate + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMyAuthor() {
		return myAuthor;
	}

	public void setMyAuthor(int myAuthor) {
		this.myAuthor = myAuthor;
	}

	public int getMyPost() {
		return myPost;
	}

	public void setMyPost(int myPost) {
		this.myPost = myPost;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}
}
