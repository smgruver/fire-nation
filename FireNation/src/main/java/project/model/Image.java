package project.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="image")
public class Image {

	@Id
	@Column(name="image_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
//	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
//	@JoinColumn(name="post_id")
	@Column(name="post_id")
	private int myPost;
	
	@Column(name="post_pic")
	private String source;
	
	public Image() {
		// TODO Auto-generated constructor stub
	}

	//New Image
	public Image(int myPost, String source) {
		super();
		this.myPost = myPost;
		this.source = source;
	}
	
	//Existing Image
	public Image(int id, int myPost, String source) {
		super();
		this.id = id;
		this.myPost = myPost;
		this.source = source;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMyPost() {
		return myPost;
	}

	public void setMyPost(int myPost) {
		this.myPost = myPost;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return "Image [id=" + id + ", myPost=" + myPost + ", source=" + source + "]";
	}
	
	
}
