package project.model;

/**
 * Used with a request to change the current User's password. Verifies current
 * user by checking oldPassword against the User record with the current User's
 * username. If they match, replaces the record's password with newPassword.
 * 
 * @author sgruv
 *
 */
public class EditUserPasswordRequestBody {

	private String oldPassword;
	private String newPassword;

	/**
	 * No args constructor used by Spring
	 */
	public EditUserPasswordRequestBody() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Accepts the old password and the new password. The oldPassword will be
	 * checked against the currentUser's current password. If they match, the
	 * password will be replaced by newPassword.
	 * 
	 * @param oldPassword
	 * @param newPassword
	 */
	public EditUserPasswordRequestBody(String oldPassword, String newPassword) {
		super();
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Override
	public String toString() {
		return "EditUserPasswordRequestBody [oldPassword=" + oldPassword + ", newPassword=" + newPassword + "]";
	}
}
