package project.model;

import java.util.List;

/**
 * Contains all of the information necessary for printing a complete view of a {@link Post} object.
 * Includes the {@link Post} object, and a {@link List} field containing {@link Comment} objects (currently unused).
 * @author sgruv
 *
 */
public class PostPage {

	private Post myPost;
	
	private List<Comment> postComments;
	
	/**
	 * No args constructor used by Spring
	 */
	public PostPage() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Accepts a post and a list of comments (currently unused) for the front end to display a complete Post view.
	 * @param myPost
	 * @param postComments
	 */
	public PostPage(Post myPost, List<Comment> postComments) {
		super();
		this.myPost = myPost;
		this.postComments = postComments;
	}

	public Post getMyPost() {
		return myPost;
	}

	public void setMyPost(Post myPost) {
		this.myPost = myPost;
	}

	public List<Comment> getPostComments() {
		return postComments;
	}

	public void setPostComments(List<Comment> postComments) {
		this.postComments = postComments;
	}

	@Override
	public String toString() {
		return "PostPage [myPost=" + myPost + ", postCOmments=" + postComments + "]";
	}
}
