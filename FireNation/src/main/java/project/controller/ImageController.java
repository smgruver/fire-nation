package project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import project.model.Image;
import project.service.ImageService;

//@RequestMapping("/images")
//@Controller
//@CrossOrigin(origins="*")
public class ImageController {

	private ImageService imageServ;
	
	//CONSTRUCTORS
	public ImageController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public ImageController(ImageService imageServ) {
		super();
		this.imageServ = imageServ;
	}
	
	//METHODS
	@GetMapping(value = "/getAllImages")
	public @ResponseBody List<Image> getAllImages() {
		return imageServ.getAllImages();
	}
	
	@GetMapping(value = "/getImageById")
	public @ResponseBody Image getImageById(@RequestParam("id") int id) {
		return imageServ.getImageById(id);
	}
	
	@GetMapping(value = "/getImagesByPost")
	public @ResponseBody List<Image> getImagesByPost(@RequestParam("post") int postId) {
		return imageServ.getImagesByPostId(postId);
	}
	
	@PostMapping(value = "/addImageToPost")
	public @ResponseBody String addImageToPost(@RequestBody Image incomingImage) {
		imageServ.addImage(incomingImage);
		return "Success";
	}
	
	@DeleteMapping(value = "/removeImage")
	public @ResponseBody String removeImageFromPost(@RequestBody Image incomingImage) {
		imageServ.removeImage(incomingImage);
		return "Success";
	}
	
	@DeleteMapping(value = "/removeImageById")
	public @ResponseBody String removeImageFromPostById(@RequestBody int imageId) {
		imageServ.removeImage(imageServ.getImageById(imageId));
		return "Success";
	}
}
