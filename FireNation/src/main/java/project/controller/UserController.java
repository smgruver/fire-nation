package project.controller;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import project.exception.RecordNotFoundException;
import project.model.EditUserBody;
import project.model.EditUserPasswordRequestBody;
import project.model.JsonSingleInput;
import project.model.SessionUser;
import project.model.User;
import project.model.UserPage;
import project.service.EmailSenderService;
import project.service.UserService;

/**
 * Controller Component for Sping to accept and handle User-oriented requests
 * made to the API. Endpoints in this controller have a prefix of "/users"
 * 
 * @author sgruv
 *
 */
@RequestMapping("/users")
@Controller
@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
public class UserController {

	// FIELDS
	private UserService userServ;

	// CONSTRUCTORS
	/**
	 * No args constructor used by Spring to set up for autowiring.
	 */
	public UserController() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor accepts a {@link UserService} class, expected to be an autowired
	 * Spring Bean
	 * 
	 * @param userServ
	 */
	@Autowired
	public UserController(UserService userServ) {
		super();
		this.userServ = userServ;
	}

	// METHODS
	/**
	 * Returns all User records from the database. That's about it.
	 * 
	 * @return
	 */
	@ResponseStatus(value = HttpStatus.OK)
	@GetMapping(value = "/getAllUsers")
	public @ResponseBody List<User> getAllUsers() {
		return userServ.getAllUsers();
	}

	/**
	 * Accepts an int 'id' as a URI-line request parameter to find a {@ User} with
	 * matching Id. Responds with 404 if no user is found. Returns the User object.
	 * 
	 * @param response
	 * @param num
	 * @return
	 * @throws RecordNotFoundException
	 */
	@GetMapping(value = "getUserById")
	public @ResponseBody User getUserById(HttpServletResponse response, @RequestParam("id") int num)
			throws RecordNotFoundException {

		User target = userServ.getUserById(num);

		return target;
	}

	/**
	 * Accepts a String 'username' as a URI-line request parameter to find a {@
	 * User} with matching username. Responds with 404 if no user is found. Returns
	 * the User object.
	 * 
	 * @param username
	 * @return
	 * @throws RecordNotFoundException
	 */
	@GetMapping(value = "getUserByUsername")
	public @ResponseBody User getUserByUsername(HttpServletResponse response, @RequestParam("username") String username)
			throws RecordNotFoundException {

		return userServ.getUserByUsername(username);
	}

	/**
	 * Accepts a String 'email' as a URI-line request parameter to find a {@ User}
	 * with matching email. Responds with 404 if no user is found. Returns the User
	 * object.
	 * 
	 * @param email
	 * @return
	 * @throws RecordNotFoundException
	 */
	@GetMapping(value = "getUserByEmail")
	public @ResponseBody User getUserByEmail(@RequestParam("email") String email) throws RecordNotFoundException {
		return userServ.getUserByEmail(email);
	}

	/**
	 * Returns a {@link UserPage} for the User with the given URI-line parameter
	 * 'id'. Contains the {@link User}, the {@link Post} objects authored by the
	 * given User, and Posts liked by the given User (or null, right now.)
	 * 
	 * @param num
	 * @return
	 * @throws RecordNotFoundException
	 */
	@GetMapping(value = "/getUserPage")
	public @ResponseBody UserPage getUserPageInfo(@RequestParam("id") int num) throws RecordNotFoundException {
		UserPage result = userServ.getUserPageById(num);

		return result;
	}

	/**
	 * Can be used to create a new User. Use {@link SessionController} method
	 * newUser with endpoint "/newUser" instead.
	 * 
	 * @param incomingUser
	 * @return
	 */
	@Deprecated
	@PostMapping(value = "/createUser")
	public @ResponseBody String createNewUser(@RequestBody User incomingUser) {
		userServ.addUser(incomingUser);
		return "Success";
	}

	/**
	 * Accepts an {@link EdituserBody} object with potentially new values for fields
	 * of the {@link User} object corresponding to the current user. Any values set
	 * to null will be ignored and left the same.
	 * 
	 * @param session
	 * @param response
	 * @param changedUser
	 * @return
	 */
	@PatchMapping(value = "/editUser")
	public @ResponseBody String editUser(HttpSession session, HttpServletResponse response,
			@RequestBody EditUserBody changedUser) {
		SessionUser currentUser = (SessionUser) session.getAttribute("currentUser");

		if (currentUser == null) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You need to be logged in to like a post!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "Not logged in!";
		}

		User target = userServ.getUserByUsername(currentUser.getUsername());
		User changedTarget = userServ.editUser(target, changedUser);
		
		session.removeAttribute("currentUser");
		session.setAttribute("currentUser", new SessionUser(changedTarget.getUsername(), changedTarget.getPassword()));
		
		return "Success";
	}
	
	@PatchMapping(value = "/editUserPassword")
	public @ResponseBody boolean editUserPassword(HttpSession session, HttpServletResponse response, @RequestBody EditUserPasswordRequestBody body) {
		SessionUser currentUser = (SessionUser) session.getAttribute("currentUser");

		if (currentUser == null) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You need to be logged in to like a post!");
			} catch (IOException e) {
				response.setStatus(HttpStatus.UNAUTHORIZED.value());
				e.printStackTrace();
			}

			return false;
		}
		
		boolean didChange = userServ.editUserPassword(userServ.getUserByUsername(currentUser.getUsername()), body);
		
		if (didChange) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Uses {@link EmailSenderService} to replace the user's password with a random String and email that string to the given email address.
	 * Randomly generates a 10-character alphanumeric String, finds a User with a 
	 * @param email
	 * @return
	 */
	@PatchMapping(value = "/resetPassword")
	public @ResponseBody boolean resetPassword(HttpServletResponse response, @RequestBody JsonSingleInput<String> email) {
		User target = userServ.getUserByEmail(email.getInput());
		
		if (target == null) {
			return false;
		}
		
		String randomPassword = userServ.resetPassword(target);

		String mailMessage = "A password reset was requested for your FlameBook account. "
				+ "Your temporary password is \n" + randomPassword
				+ "\n\nLog in with this password and then change your password from your User page.";

		try {
			EmailSenderService.sendMail(email.getInput(), mailMessage);
		} catch (MessagingException e) {
			System.out.println("Error with sending the email.");
			return false;
		}

		return true;

	}

	/**
	 * Deletes the account of the current user (and invalidates their session).
	 * @param session
	 * @param response
	 * @return
	 */
	@DeleteMapping(value = "/deleteCurrentUser")
	public @ResponseBody String deleteUser(HttpSession session, HttpServletResponse response) {
		SessionUser currentUser = (SessionUser) session.getAttribute("currentUser");

		if (currentUser == null) {
			try {
				response.sendError(HttpStatus.UNAUTHORIZED.value(), "You need to be logged in to delete yourself!");
			} catch (IOException e) {
				e.printStackTrace();
			}

			return "Not logged in!";
		}

		session.invalidate();
		userServ.removeUser(userServ.getUserByUsername(currentUser.getUsername()));
		return "Success";
	}

}
