package project.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.model.SessionUser;
import project.model.User;
import project.service.UserService;

/**
 * Facilitates the API interface for endpoints related to logging in or managing the current user's session.
 * Endpoints from this controller have no prefix.
 * @author sgruv
 *
 */
@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
@RestController
public class SessionController {

	private UserService userServ;
	
	/**
	 * No args constructor used by Spring to set up for autowiring.
	 */
	public SessionController() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructor accepts a {@link UserService} class, expected to be an autowired Spring Bean
	 * @param userServ
	 */
	@Autowired
	public SessionController(UserService userServ) {
		super();
		this.userServ = userServ;
	}
	
	/**
	 * Returns the SessionUser stored in the session.
	 * @param request
	 * @param response
	 * @return
	 */
	@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
	@GetMapping(value="/getUser")
	public User getLoggedInUser(HttpServletRequest request, HttpServletResponse response) {
		SessionUser currentSessionUser = (SessionUser) request.getSession().getAttribute("currentUser");
		
		System.out.println((currentSessionUser != null) ? "/getUser:" + currentSessionUser.getUsername() : "/getUser: User not logged in");
		
		if (currentSessionUser == null) {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return null;
		}
		
		User currentUser = userServ.getUserByUsername(currentSessionUser.getUsername());
//		System.out.println((currentUser != null) ? currentUser.getUsername() : "/getUser: No users matching this SessionUser");
		
		return currentUser;
	}
	
	/**
	 * Accepts a SessionUser parameter username & password to be compared to User records' usernames and passwords.
	 * If a match is found, saves the SessionUser to the current session to be used for validation.
	 * @param request
	 * @param response
	 * @param incomingUser
	 * @return
	 */
	@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
	@PostMapping(value = "/login")
	public User login(HttpServletRequest request, HttpServletResponse response, @RequestBody SessionUser incomingUser) {
		SessionUser currentUser = (SessionUser)request.getSession().getAttribute("currentUser");
//		System.out.println((currentUser != null) ? "/login:" + currentUser.getUsername() : "/login: User not logged in");
		
		if (currentUser != null) {
			try {
				response.sendError(HttpStatus.BAD_REQUEST.value(), "You are already logged in. Please log out first!");
			} catch (IOException e) {
				e.printStackTrace();
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				return null;
			}
			
			return null;
		}

		User loggedUser = userServ.loginUser(incomingUser);
//		System.out.println((loggedUser != null) ? loggedUser.getUsername() : "/login: No users matching this SessionUser");
		
		if (loggedUser != null) {
			request.getSession().setAttribute("currentUser", incomingUser);
//			System.out.println((request.getSession().getAttribute("currentUser") != null) ? request.getSession().getAttribute("currentUser") : "/login: SessionUser not saved to session");
			return loggedUser;
		} else {
			//response.setStatus(HttpStatus.NOT_FOUND.value());
			return null;
		}
	}
	
	/**
	 * Accepts a User object that will be used to create a new user.
	 * Throws an exception if the current user's session has a variable stored to it.
	 * @param request
	 * @param response
	 * @param newAccount
	 * @return
	 * @throws IOException
	 */
	@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
	@PostMapping(value = "/newUser")
	public boolean newuser(HttpServletRequest request, HttpServletResponse response, @RequestBody User newAccount) throws IOException {
		System.out.println(newAccount);
		
		if (request.getSession().getAttribute("currentUser") != null) {
			response.sendError(HttpStatus.BAD_REQUEST.value(), "You are already logged in. Please log out first!");
		}
		
		User sameUsername = userServ.getUserByUsername(newAccount.getUsername());
		User sameEmail = userServ.getUserByEmail(newAccount.getEmail());
		
		if (sameUsername != null || sameEmail != null) {
			try {
				response.sendError(HttpStatus.BAD_REQUEST.value(), "Either email or username taken");
			} catch (Exception e) {
				e.printStackTrace();
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				return false;
			}
			
			return false;
		} else {
			userServ.addUser(newAccount);
			return true;
		}	
	}
	
	/**
	 * Invalidates the current user's session, so the user is effectively logged out.
	 * @param session
	 * @param response
	 * @return
	 */
	@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true", maxAge = 3600, allowedHeaders = "*")
	@GetMapping(value="/logout")
	public String logout(HttpServletRequest session, HttpServletResponse response) {
//		SessionUser currentUser = (SessionUser)session.getSession().getAttribute("currentUser");
//		System.out.println((currentUser != null) ? "/logout:" + currentUser.getUsername() : "/logout: User not logged in");

		
		session.getSession().invalidate();
		return "Logged Out";
	}
}
